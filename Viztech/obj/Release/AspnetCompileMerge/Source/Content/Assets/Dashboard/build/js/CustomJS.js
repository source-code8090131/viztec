﻿//#region Custom Template Js 
$(window).on('load', function () {
    $('#loading').hide();

    if ($("#CustomModal").length > 0) {
        if ($("#CustomModal").val() != null && $("#CustomModal").val() != "") {
            $('#modal-xl').modal('show');
        }
    }
});
$(function () {
    $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#EIFHsCodes").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#EEFHsCodes").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#LoadUnpinnedGds").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#Reporting").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#Reporting_wrapper .col-md-6:eq(0)');

    //Initialize Select2 Elements
    $('.select2').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $(".closemodal").click(function (button) {
        $('#ModalContent').hide();
    });

    $("#PrintModal").click(function (button) {
        var printContents = document.getElementById('ModalContent').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    });
});

//#endregion

//#region Custom Date Picker
$(function () {
    $(".CustomDatePicker").datepicker();
});
//#endregion

//#region Group Role Active/De-active
$("[name='ActivateGroup']").click(function (button) {
    var R_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Roles/ActivateGroupRole?R_ID=' + R_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='DeactivateGroup']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to deactive?");
    if (check == true) {
        $.ajax
            ({
                url: '/Roles/DeactivateGroupRole?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region User Active/De-active
$("[name='ActivateRole']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Users/ActivateNonAuthorizeUser?R_ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='DeactivateRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to deactive?");
    if (check == true) {
        $.ajax
            ({
                url: '/Users/DeactivateAuthorizeUser?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Get Finger Print

$("[name='CallSGIFPGetData']").click(function (button) {
    CallSGIFPGetData(SuccessFunc1, ErrorFunc);
});


var secugen_lic = "";

function ErrorCodeToString(ErrorCode) {
    var Description;
    switch (ErrorCode) {
        // 0 - 999 - Comes from SgFplib.h
        // 1,000 - 9,999 - SGIBioSrv errors
        // 10,000 - 99,999 license errors
        case 51:
            Description = "System file load failure";
            break;
        case 52:
            Description = "Sensor chip initialization failed";
            break;
        case 53:
            Description = "Device not found";
            break;
        case 54:
            Description = "Fingerprint image capture timeout";
            break;
        case 55:
            Description = "No device available";
            break;
        case 56:
            Description = "Driver load failed";
            break;
        case 57:
            Description = "Wrong Image";
            break;
        case 58:
            Description = "Lack of bandwidth";
            break;
        case 59:
            Description = "Device Busy";
            break;
        case 60:
            Description = "Cannot get serial number of the device";
            break;
        case 61:
            Description = "Unsupported device";
            break;
        case 63:
            Description = "SgiBioSrv didn't start; Try image capture again";
            break;
        default:
            Description = "Unknown error code or Update code to reflect latest result" + ErrorCode;
            break;
    }
    return Description;
}

var templates = [];
var template_2 = "";
var count = 0;

var TempforServer = "";
function SuccessFunc1(result) {
    if (result.ErrorCode == 0) {
        /* 	Display BMP data in image tag
            BMP data is in base 64 format
        */
        if (result != null && result.BMPBase64.length > 0) {
            //< !--alert(result.TemplateBase64); --> /*(result.BMPBase64);*/
            TempforServer = result.TemplateBase64; /*result.BMPBase64;*/
            $("#ServerData").val(TempforServer);
            //SetSession();

            document.getElementById('FPImage1').src = "data:image/bmp;base64," + result.BMPBase64;
        }
        templates[count] = result.TemplateBase64;
        count = count + 1;
    }
    else {
        alert("Fingerprint Capture Error Code:  " + result.ErrorCode + ".\nDescription:  " + ErrorCodeToString(result.ErrorCode) + ".");
    }
}

function SuccessFunc2(result) {
    if (result.ErrorCode == 0) {
        /* 	Display BMP data in image tag
            BMP data is in base 64 format
        */
        if (result != null && result.BMPBase64.length > 0) {
            document.getElementById('FPImage2').src = "data:image/bmp;base64," + result.BMPBase64;
        }
        template_2 = result.TemplateBase64;
    }
    else {
        alert("Fingerprint Capture Error Code:  " + result.ErrorCode + ".\nDescription:  " + ErrorCodeToString(result.ErrorCode) + ".");
    }
}

function ErrorFunc(status) {
    /*
        If you reach here, user is probabaly not running the
        service. Redirect the user to a page where he can download the
        executable and install it.
    */
    alert("Check if SGIBIOSRV is running; status = " + status + ":");
}

function CallSGIFPGetData(successCall, failCall) {
    //var curdate = new Date();
    //document.getElementById("ContentPlaceHolder1_txtActivityDateTime").value = curdate.toString();
    var uri = "https://localhost:8443/SGIFPCapture";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            fpobject = JSON.parse(xmlhttp.responseText);
            successCall(fpobject);
        }
        else if (xmlhttp.status == 404) {
            failCall(xmlhttp.status)
        }
    }
    xmlhttp.onerror = function () {
        failCall(xmlhttp.status);
    }
    var params = "Timeout=" + "10000";
    params += "&Quality=" + "50";
    params += "&licstr=" + encodeURIComponent(secugen_lic);
    params += "&templateFormat=" + "ISO";
    xmlhttp.open("POST", uri, true);
    xmlhttp.send(params);
}

function matchScore(succFunction, failFunction) {
    //if (template_1 == "" || template_2 == "") {
    //    alert("Please scan two fingers to verify!!");
    //    return;
    //}

    for (var i = 0; i < templates.length; i++) {

        var uri = "https://localhost:8443/SGIMatchScore";
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                fpobject = JSON.parse(xmlhttp.responseText);
                succFunction(fpobject, i);
            }
            else if (xmlhttp.status == 404) {
                failFunction(xmlhttp.status)
            }
        }

        xmlhttp.onerror = function () {
            failFunction(xmlhttp.status);
        }
        var params = "template1=" + encodeURIComponent(template_2);
        params += "&template2=" + encodeURIComponent(templates[i]);
        params += "&licstr=" + encodeURIComponent(secugen_lic);
        params += "&templateFormat=" + "ISO";
        //xmlhttp.open("POST", uri, false);
        xmlhttp.send(params);
    }
}

function succMatch(result, index) {
    var idQuality = document.getElementById("quality").value;
    if (result.ErrorCode == 0) {
        if (result.MatchingScore >= idQuality)
            alert("MATCHED ! (" + result.MatchingScore + ") at Index Person : " + index);
        //else
        //    alert("NOT MATCHED ! (" + result.MatchingScore + ")");
    }
    else {
        alert("Error Scanning Fingerprint ErrorCode = " + result.ErrorCode);
    }
}

function failureFunc(error) {
    alert("On Match Process, failure has been called");
}


function SetSession() {


    var value = TempforServer;
    var arr = { value: value };

    $.ajax({
        type: "POST",
        url: "/Verification/SetSession",
        async: false,
        data: JSON.stringify(arr),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: OnFailure,
        Error: Error
    });
}


function OnSuccess(response) {
    //alert(response.d);
    //alert("Ajax Call Successfull");

}
function OnFailure(response) {
    //alert("Ajax Call Failed");
}

function Error(response) {
    //alert("Ajax Call Error");
}


//#endregion

//#region Send Request To NADRA
$("#btnrequest").click(function (button) {
    var CitizenNo = $("#CitizenNumber").val();
    var ContactNo = $("#ContactNumber").val();
    alert("Citizen Number : " + CitizenNo + " Contact Number : " + ContactNo);
    //$.ajax
    //    ({
    //        url: '/Verification/Biometric',
    //        type: 'POST',
    //        datatype: 'application/json',
    //        contentType: 'application/json',
    //        beforeSend: function () {
    //            $('#loading').show();
    //        },
    //        complete: function () {
    //            $('#loading').hide();
    //        },
    //        success: function () {
    //        },
    //        error: function () {
    //            //alert("Something went wrong..");
    //        },
    //    });
});
//#endregion

//#region Browser Back Button
$("[name='BrowserBack']").click(function (button) {
    history.back();
});
//#endregion


