﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;

namespace Viztech.Controllers
{
    public class UsersController : Controller
    {
        ViztecEntities context = new ViztecEntities();
        // GET: Users
        public ActionResult Index()
        {
            return View();
        }
        #region Add Users
        
        public ActionResult AddUser(int L = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string PreviousRoles = "";
                    try
                    {
                        if (L != 0)
                        {
                            AssignRoles_Custom entity = new AssignRoles_Custom();
                            entity.Entity = context.BIO_LOGIN.Where(m => m.ID == L).FirstOrDefault();
                            if (entity != null)
                            {
                                if (entity.Entity != null)
                                {
                                    PreviousRoles = context.BIO_LOGIN_ROLES.Where(m => m.ID == entity.Entity.ID).Select(m => m.GroupName).FirstOrDefault();
                                }
                                string[] RoleName = PreviousRoles.Split(',');
                                List<SelectListItem> items = new SelectList(context.BIO_ROLES.Where(m => m.GROUP_STATUS == "Active").OrderBy(m => m.BIO_ROLE_ID).ToList(), "Role_Name", "Role_Name", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    if (RoleName.Contains(item.Value))
                                        item.Selected = true;
                                }
                                entity.RolesList = items;
                                return View(entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + L;
                                return View();
                            }
                        }
                        else
                        {
                            AssignRoles_Custom entity = new AssignRoles_Custom();
                            entity.Entity = new BIO_LOGIN();
                            entity.Entity.ID = 0;
                            List<SelectListItem> items = new SelectList(context.BIO_ROLES.Where(m => m.GROUP_STATUS == "Active").OrderBy(m => m.BIO_ROLE_ID).ToList(), "Role_Name", "Role_Name", 0).ToList();
                            entity.RolesList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Users", "AddUser", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("UnAuthorizedUrl", "Error");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddUser(AssignRoles_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string NewUserID = "";
                    try
                    {
                        BIO_LOGIN UpdateEntity = new BIO_LOGIN();
                        BIO_LOGIN CheckIfExist = new BIO_LOGIN();
                        bool Checkifnew = false;

                        if (db_table.Entity.ID == 0)
                        {
                            CheckIfExist = context.BIO_LOGIN.Where(m => m.ID != db_table.Entity.ID && (m.USER_ID.ToLower() == db_table.Entity.EMAIL.ToLower())).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                //UpdateEntity.ID = Convert.ToInt32(context.BIO_LOGIN.Max(m => (decimal?)m.ID)) + 1;
                                UpdateEntity.DateAdded = DateTime.Now;
                                UpdateEntity.USER_NAME = db_table.Entity.USER_NAME;
                                UpdateEntity.EMAIL = db_table.Entity.EMAIL;
                                NewUserID = db_table.Entity.EMAIL;
                                if (UpdateEntity.EMAIL == "Admin")
                                    UpdateEntity.PASSWORD = DAL.Encrypt("Citibank1!");
                                else
                                    UpdateEntity.PASSWORD = DAL.Encrypt("12345");
                                UpdateEntity.TYPE = db_table.Entity.TYPE;
                                UpdateEntity.AddUser = Session["USER_ID"].ToString();
                                if (UpdateEntity.AddUser == "Admin")
                                {
                                    UpdateEntity.Activator_ID = UpdateEntity.AddUser;
                                    UpdateEntity.USER_STATUS = "Active";
                                }
                                else
                                {
                                    UpdateEntity.Activator_ID = null;
                                    UpdateEntity.USER_STATUS = "DeActive";
                                }
                                UpdateEntity.USER_ID = db_table.Entity.EMAIL;
                                UpdateEntity.Activity = "Maker";
                                context.BIO_LOGIN.Add(UpdateEntity);
                                Checkifnew = true;
                            }
                            else
                            {
                                message = "User ID " + CheckIfExist.EMAIL + " is already exist in the system";
                            }
                        }
                        else
                        {
                            UpdateEntity = context.BIO_LOGIN.Where(m => m.ID == db_table.Entity.ID).FirstOrDefault();
                            UpdateEntity.USER_NAME = db_table.Entity.USER_NAME;
                            UpdateEntity.EMAIL = db_table.Entity.EMAIL;
                            UpdateEntity.AddUser = Session["USER_ID"].ToString();
                            if (UpdateEntity.AddUser == "Admin")
                            {
                                UpdateEntity.Activator_ID = UpdateEntity.AddUser;
                                UpdateEntity.USER_STATUS = "Active";
                            }
                            else
                            {
                                UpdateEntity.Activator_ID = null;
                                UpdateEntity.USER_STATUS = "DeActive";
                            }
                            if (UpdateEntity.EMAIL == "Admin")
                                UpdateEntity.PASSWORD = DAL.Encrypt("Citibank1!");
                            else
                                UpdateEntity.PASSWORD = DAL.Encrypt("12345");
                            UpdateEntity.TYPE = db_table.Entity.TYPE;
                            UpdateEntity.USER_ID = db_table.Entity.EMAIL;
                            UpdateEntity.Activity = "Maker";

                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        }
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            #region Get Groups for Child Table
                            string RoleID = "";
                            foreach (string SelectedRolesId in db_table.SelectedRoles)
                            {
                                BIO_ROLES GetRole = context.BIO_ROLES.Where(m => m.Role_Name == SelectedRolesId).FirstOrDefault();
                                if (GetRole != null)
                                    RoleID += GetRole.BIO_ROLE_ID + ",";
                            }
                            RoleID = RoleID.Substring(0, (RoleID.Length - 1));

                            var SelectedRoles = String.Join(",", db_table.SelectedRoles);

                            BIO_LOGIN_ROLES RolesToUpdate = context.BIO_LOGIN_ROLES.Where(m => m.ID == db_table.Entity.ID && m.USER_NAME == db_table.Entity.USER_NAME).FirstOrDefault();
                            if (RolesToUpdate == null)
                            {
                                RolesToUpdate = new BIO_LOGIN_ROLES();
                                //RolesToUpdate.Role_ID = Convert.ToInt32(context.BIO_LOGIN_ROLES.Max(m => (decimal?)m.Role_ID)) + 1;
                                RolesToUpdate.USER_NAME = db_table.Entity.USER_NAME;
                                RolesToUpdate.USER_TYPE = db_table.Entity.TYPE;
                                RolesToUpdate.Roles_Assign = RoleID;
                                RolesToUpdate.ID = UpdateEntity.ID;
                                RolesToUpdate.GroupName = SelectedRoles;
                                context.BIO_LOGIN_ROLES.Add(RolesToUpdate);
                            }
                            else
                            {
                                RolesToUpdate.USER_NAME = db_table.Entity.USER_NAME;
                                RolesToUpdate.USER_TYPE = db_table.Entity.TYPE;
                                RolesToUpdate.Roles_Assign = RoleID;
                                RolesToUpdate.ID = db_table.Entity.ID;
                                RolesToUpdate.GroupName = SelectedRoles;
                                context.BIO_LOGIN_ROLES.Add(RolesToUpdate);
                                context.Entry(RolesToUpdate).State = EntityState.Modified;
                            }
                            #endregion

                            RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                RowsAffected = 0;
                                #region Add User to User Log
                                User_TBL_ADD_LOG UserLog = new User_TBL_ADD_LOG();
                                UserLog.USERID = db_table.Entity.EMAIL;
                                UserLog.USERNAME = db_table.Entity.USER_NAME;
                                UserLog.USER_TYPE = db_table.Entity.TYPE;
                                UserLog.Roles_Assign = RolesToUpdate.GroupName;
                                UserLog.Adding_User_IP = DAL.GetIP();
                                UserLog.MakerTime = DateTime.Now;
                                UserLog.BIO_ID = UpdateEntity.ID;
                                UserLog.Added_by = Session["USER_ID"].ToString();
                                if (UserLog.Added_by == "Admin")
                                {
                                    UserLog.CheckerTime = DateTime.Now;
                                    UserLog.CurrentStatus = "Active";
                                    UserLog.Activate_by = UserLog.Added_by;
                                }
                                else
                                {
                                    UserLog.CheckerTime = null;
                                    UserLog.CurrentStatus = "DeActive";
                                    UserLog.Activate_by = null;
                                }
                                if (Checkifnew == true)
                                    UserLog.Description = "Added a New User With Roles " + RolesToUpdate.GroupName;
                                else
                                    UserLog.Description = "No New Change and Active the User";
                                context.User_TBL_ADD_LOG.Add(UserLog);
                                #endregion
                                RowsAffected = context.SaveChanges();
                                if (RowsAffected > 0)
                                {
                                    USER_PROFILE_DETAILS UserProfile = context.USER_PROFILE_DETAILS.Where(m => m.USERID == UpdateEntity.EMAIL).FirstOrDefault();
                                    if (UserProfile == null)
                                    {
                                        UserProfile = new USER_PROFILE_DETAILS();
                                        UserProfile.USERID = UpdateEntity.EMAIL;
                                        UserProfile.USERNAME = UpdateEntity.USER_NAME;
                                        UserProfile.USER_DESIGNATION = null;
                                        UserProfile.USER_IMAGE = null;
                                        UserProfile.LOGIN_ID = UpdateEntity.ID.ToString();
                                        UserProfile.ROLES_ID = RolesToUpdate.Role_ID.ToString();
                                        context.USER_PROFILE_DETAILS.Add(UserProfile);
                                    }
                                    else
                                    {
                                        UserProfile.USERID = UpdateEntity.EMAIL;
                                        UserProfile.USERNAME = UpdateEntity.USER_NAME;
                                        UserProfile.USER_DESIGNATION = null;
                                        UserProfile.USER_IMAGE = null;
                                        context.Entry(UserProfile).State = EntityState.Modified;
                                    }
                                    RowsAffected = context.SaveChanges();
                                    //if (RowsAffected > 0)
                                    //{
                                    //    BIO_LOGIN_STATUS Status = context.BIO_LOGIN_STATUS.Where(m => m.USERNAME == UpdateEntity.EMAIL).FirstOrDefault();
                                    //    if (Status == null)
                                    //    {
                                    //        Status = new BIO_LOGIN_STATUS();
                                    //        RowsAffected = 0;
                                    //        Status.USERNAME = UpdateEntity.EMAIL;
                                    //        Status.BIO_LOGIN_ID = UpdateEntity.ID;
                                    //        Status.USER_STATUS = false;
                                    //        Status.AddUser = Session["USER_ID"].ToString();
                                    //        context.BIO_LOGIN_STATUS.Add(Status);
                                    //        RowsAffected = context.SaveChanges();
                                    //    }
                                    //}
                                    if (RowsAffected > 0)
                                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                }
                            }
                        }
                        else
                        {
                            if (message == "")
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Users", "AddUser", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    string PreviousRoles = "";
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    entity.Entity = new BIO_LOGIN();
                    entity.Entity.ID = db_table.Entity.ID;
                    if (db_table.Entity.ID != 0)
                        PreviousRoles = context.BIO_LOGIN_ROLES.Where(m => m.ID == db_table.Entity.ID).Select(m => m.GroupName).FirstOrDefault();
                    else
                    {
                        entity.Entity = context.BIO_LOGIN.Where(m => m.USER_ID == NewUserID).FirstOrDefault();
                        if (entity != null)
                        {
                            if (entity.Entity != null)
                            {
                                PreviousRoles = context.BIO_LOGIN_ROLES.Where(m => m.ID == entity.Entity.ID).Select(m => m.GroupName).FirstOrDefault();
                            }
                        }
                    }
                    string[] RoleName = PreviousRoles.Split(',');
                    List<SelectListItem> items = new SelectList(context.BIO_ROLES.Where(m => m.GROUP_STATUS == "Active").OrderBy(m => m.BIO_ROLE_ID).ToList(), "Role_Name", "Role_Name", 0).ToList();
                    foreach (SelectListItem item in items)
                    {
                        if (RoleName.Contains(item.Value))
                            item.Selected = true;
                    }
                    entity.RolesList = items;
                    return View(entity);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult AddUserList()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<BIO_LOGIN_VIEW> ViewData = context.BIO_LOGIN_VIEW.Where(m => m.USER_STATUS == "Active" ).OrderByDescending(m => m.ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }

        #endregion

        #region Active Users
        public ActionResult AuthorizeUser()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Users", "AuthorizeUser", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<BIO_LOGIN_VIEW> ViewData = context.BIO_LOGIN_VIEW.Where(m => m.USER_STATUS == "Active").OrderByDescending(m => m.ID).ToList();
                    return View(ViewData);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public JsonResult DeactivateAuthorizeUser(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Users", "AuthorizeUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        BIO_LOGIN UpdateEntity = context.BIO_LOGIN.Where(m => m.ID == R_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.USER_STATUS = "DeActive";
                            UpdateEntity.Activity = "Checker";
                            UpdateEntity.Activator_ID = Session["USER_ID"].ToString();
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                #region Group Role Log
                                BIO_LOGIN_ACTIVATION_LOG_TBL UserLog = new BIO_LOGIN_ACTIVATION_LOG_TBL();
                                UserLog.USERNAME = UpdateEntity.USER_NAME;
                                UserLog.EMAIL = UpdateEntity.EMAIL;
                                UserLog.USER_STATUS = UpdateEntity.USER_STATUS;
                                UserLog.DATE_TIME = DateTime.Now;
                                UserLog.IP = DAL.GetIP();
                                string LoggedinUser = Session["USER_ID"].ToString();
                                UserLog.ACTIVATOR_ID = LoggedinUser;
                                UserLog.Description = "Roles Removed & DeActive the User";
                                context.BIO_LOGIN_ACTIVATION_LOG_TBL.Add(UserLog);
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "User : " + UpdateEntity.USER_NAME + "  Deactive";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Users", "DeactivateAuthorizeUser", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        #endregion

        #region De-active Users
        public ActionResult NonAuthorizeUser()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Users", "NonAuthorizeUser", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<BIO_LOGIN_VIEW> ViewData = context.BIO_LOGIN_VIEW.Where(m => m.AddUser != SessionUser && m.USER_STATUS == "DeActive").OrderByDescending(m => m.ID).ToList();
                    return View(ViewData);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public JsonResult ActivateNonAuthorizeUser(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Users", "NonAuthorizeUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        BIO_LOGIN UpdateEntity = context.BIO_LOGIN.Where(m => m.ID == R_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.USER_STATUS = "Active";
                            UpdateEntity.Activity = "Checker";
                            UpdateEntity.Activator_ID = Session["USER_ID"].ToString();
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                #region Group Role Log
                                BIO_LOGIN_ACTIVATION_LOG_TBL UserLog = new BIO_LOGIN_ACTIVATION_LOG_TBL();
                                UserLog.USERNAME = UpdateEntity.USER_NAME;
                                UserLog.EMAIL = UpdateEntity.EMAIL;
                                UserLog.USER_STATUS = UpdateEntity.USER_STATUS;
                                UserLog.DATE_TIME = DateTime.Now;
                                UserLog.IP = DAL.GetIP();
                                string LoggedinUser = Session["USER_ID"].ToString();
                                UserLog.ACTIVATOR_ID = LoggedinUser;
                                UserLog.Description = "Active the User & No New Change";
                                context.BIO_LOGIN_ACTIVATION_LOG_TBL.Add(UserLog);
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "User : " + UpdateEntity.USER_NAME + " is successfully Activated";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Users", "ActivateNonAuthorizeUser", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion
    }
}