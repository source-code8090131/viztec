﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;

namespace Viztech.Controllers
{
    public class MisReportController : Controller
    {
        //ViztecEntities context = new ViztecEntities();
        // GET: MisReport
        #region Group Summary
        public ActionResult GroupSummary()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "GroupSummary", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            var Data = context.SP_BIO_GET_GIDA_GROUP_DETAILS_ADMIN(null, null, "ALL").ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.Added_by != "Admin" && m.ACTIVATOR_ID != "Admin").ToList();
                                if (Data.Count() > 0)
                                {
                                    return View(Data);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "GroupSummary", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult GroupSummary(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "GroupSummary", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            if (FromDate != null && ToDate != null)
                            {
                                //string Fromdate = FromDate.ToString();
                                //string Todate = ToDate.ToString();
                                var Data = context.SP_BIO_GET_GIDA_GROUP_DETAILS_ADMIN(FromDate, ToDate, "Date").ToList();
                                if (Data.Count() > 0)
                                {
                                    Data = Data.Where(m => m.Added_by != "Admin" && m.ACTIVATOR_ID != "Admin").ToList();
                                    if (Data.Count() > 0)
                                    {
                                        return View(Data);
                                    }
                                    else
                                        message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");

                                }
                                else
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                }
                            }
                            else
                            {
                                if (FromDate == null && ToDate == null)
                                    message = "Select start date and end date to continue";
                                else if (FromDate == null)
                                    message = "Select start date to continue";
                                else if (ToDate == null)
                                    message = "Select end date to continue";
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "GroupSummary", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        #region User Details Summary
        public ActionResult UserSummary()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "UserSummary", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            var Data = context.SP_BIO_GET_GIDA_USER_DETAILS_ADMIN(null, null, "ALL").ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.AddUser != "Admin" && m.ACTIVATOR_ID != "Admin").ToList();
                                if (Data.Count() > 0)
                                {
                                    return View(Data);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "UserSummary", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UserSummary(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "UserSummary", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            if (FromDate != null && ToDate != null)
                            {
                                //string Fromdate = FromDate.ToString();
                                //string Todate = ToDate.ToString();
                                var Data = context.SP_BIO_GET_GIDA_USER_DETAILS_ADMIN(FromDate, ToDate, "Date").ToList();
                                if (Data.Count() > 0)
                                {
                                    Data = Data.Where(m => m.AddUser != "Admin" && m.ACTIVATOR_ID != "Admin").ToList();
                                    if (Data.Count() > 0)
                                    {
                                        return View(Data);
                                    }
                                    else
                                        message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                }
                            }
                            else
                            {
                                if (FromDate == null && ToDate == null)
                                    message = "Select start date and end date to continue";
                                else if (FromDate == null)
                                    message = "Select start date to continue";
                                else if (ToDate == null)
                                    message = "Select end date to continue";
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "UserSummary", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        #region Custom Report
        public ActionResult Customize()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "Customize", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            var Data = context.SP_BIO_GET_GIDA_USER_DETAILS_ADMIN(null, null, "ALL").ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "Customize", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Customize(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "Customize", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            if (FromDate != null && ToDate != null)
                            {
                                //string Fromdate = FromDate.ToString();
                                //string Todate = ToDate.ToString();
                                var Data = context.SP_BIO_GET_GIDA_USER_DETAILS_ADMIN(FromDate, ToDate, "Date").ToList();
                                if (Data.Count() > 0)
                                {
                                    return View(Data);
                                }
                                else
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                }
                            }
                            else
                            {
                                if (FromDate == null && ToDate == null)
                                    message = "Select start date and end date to continue";
                                else if (FromDate == null)
                                    message = "Select start date to continue";
                                else if (ToDate == null)
                                    message = "Select end date to continue";
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "Customize", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        #region User Activity Report
        public ActionResult UserActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "UserActivity", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            var Data = context.SP_BIO_GET_GIDA_USER_ACTIVITY_ADMIN(null, null, "ALL").ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.Added_by != "Admin" && m.Activate_by != "Admin").ToList();
                                if (Data.Count > 0)
                                {
                                    return View(Data);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "UserActivity", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UserActivity(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("MisReport", "UserActivity", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            if (FromDate != null && ToDate != null)
                            {
                                //string Fromdate = FromDate.ToString();
                                //string Todate = ToDate.ToString();
                                var Data = context.SP_BIO_GET_GIDA_USER_ACTIVITY_ADMIN(FromDate, ToDate, "Date").ToList();
                                if (Data.Count() > 0)
                                {
                                    Data = Data.Where(m => m.Added_by != "Admin" && m.Activate_by != "Admin").ToList();
                                    if (Data.Count > 0)
                                    {
                                        return View(Data);
                                    }
                                    else
                                        message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                }
                            }
                            else
                            {
                                if (FromDate == null && ToDate == null)
                                    message = "Select start date and end date to continue";
                                else if (FromDate == null)
                                    message = "Select start date to continue";
                                else if (ToDate == null)
                                    message = "Select end date to continue";
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("MisReport", "UserActivity", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion
    }
}