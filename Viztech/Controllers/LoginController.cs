﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;
using System.Linq;
using System.Web.Security;

namespace Viztech.Controllers
{
    public class LoginController : Controller
    {
        ViztecEntities context = new ViztecEntities();
        // GET: Login
        public string GetIP()
        {
            string Str = "";
            Str = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return View();
        }
        [HttpPost]
        public ActionResult Index(CUSTOM_LOGIN db_table)
        {
            try
            {
                BIO_LOGIN login = context.BIO_LOGIN.Where(m => m.EMAIL == db_table.EMAIL && m.USER_STATUS == "Active").FirstOrDefault();
                if (login != null)
                {
                    BIO_LOGIN_ROLES loginRoles = context.BIO_LOGIN_ROLES.Where(m => m.ID == login.ID).FirstOrDefault();

                    Session["IP"] = GetIP();
                    if (loginRoles != null)
                    {
                        string EncryptedPass = DAL.Encrypt(db_table.PASSWORD);
                        if (!loginRoles.USER_NAME.ToLower().Contains("admin"))
                        {
                            string adPath = "";
                            string adurl = "";
                            if (db_table.DOMAIN_NAME == "EUR")
                            {
                                adPath = "LDAP://eur.nsroot.net"; //Fully-qualified Domain Name
                                adurl = "eur.nsroot.net";
                            }
                            else if (db_table.DOMAIN_NAME == "APAC")
                            {
                                adPath = "LDAP://apac.nsroot.net"; //Fully-qualified Domain Name
                                adurl = "apac.nsroot.net";
                            }
                            else if (db_table.DOMAIN_NAME == "NAM")
                            {
                                adPath = "LDAP://nam.nsroot.net"; //Fully-qualified Domain Name
                                adurl = "nam.nsroot.net";
                            }
                            else
                            {
                                ViewBag.message = "Unsuccessful login: Select domain name.";
                                return View();
                            }
                            LdapAuthentication adAuth = new LdapAuthentication(adPath);
                            bool CheckAuth = adAuth.IsAuthenticated(adurl, db_table.EMAIL, db_table.PASSWORD);
                            if (CheckAuth == true)
                            {
                                bool isCookiePersistent = User.Identity.IsAuthenticated;
                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                    (1, db_table.EMAIL, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                if (true == isCookiePersistent)
                                    authCookie.Expires = authTicket.Expiration;
                                Response.Cookies.Add(authCookie);

                                #region User Log
                                user_login_logout_log Log = new user_login_logout_log();
                                Log.SNO = Convert.ToInt32(context.user_login_logout_log.Max(m => (decimal?)m.SNO)) + 1;
                                Log.User_Email = login.EMAIL;
                                Log.Type = "LOGIN";
                                Log.Login_Time = DateTime.Now;
                                Log.USER_TYPE = login.TYPE;
                                Log.USER_IP = Session["IP"].ToString();
                                context.user_login_logout_log.Add(Log);
                                context.SaveChanges();
                                #endregion

                                Session["USER_ID"] = login.USER_NAME;
                                Session["ID"] = login.USER_ID;
                                Session["Type"] = login.TYPE;
                                Session["Roles_Assign"] = loginRoles.Roles_Assign;
                                Session["USERTYPE"] = loginRoles.USER_TYPE;
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                ViewBag.message = "Unsuccessful login: Incorrect username or password.";
                            }
                        }
                        else
                        {
                            if (login.EMAIL == db_table.EMAIL && login.PASSWORD == EncryptedPass && login.USER_STATUS == "Active")
                            {
                                bool isCookiePersistent = User.Identity.IsAuthenticated;
                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                       (1, db_table.EMAIL, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                if (true == isCookiePersistent)
                                    authCookie.Expires = authTicket.Expiration;
                                Response.Cookies.Add(authCookie);

                                #region User Log
                                user_login_logout_log Log = new user_login_logout_log();
                                Log.SNO = Convert.ToInt32(context.user_login_logout_log.Max(m => (decimal?)m.SNO)) + 1;
                                Log.User_Email = login.EMAIL;
                                Log.Type = "LOGIN";
                                Log.Login_Time = DateTime.Now;
                                Log.USER_TYPE = login.TYPE;
                                Log.USER_IP = Session["IP"].ToString();
                                context.user_login_logout_log.Add(Log);
                                context.SaveChanges();
                                #endregion

                                Session["USER_ID"] = login.USER_NAME;
                                Session["ID"] = login.USER_ID;
                                Session["Type"] = login.TYPE;
                                Session["Roles_Assign"] = loginRoles.Roles_Assign;
                                Session["USERTYPE"] = loginRoles.USER_TYPE;
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                if (login.PASSWORD != db_table.PASSWORD)
                                    ViewBag.message = "Unsuccessful login: Invalid password.";
                                else if (login.USER_STATUS != "Active")
                                    ViewBag.message = "Unsuccessful login: User " + db_table.EMAIL + " is deactive.";
                                else if (login.EMAIL != db_table.EMAIL)
                                    ViewBag.message = "Unsuccessful login: Invalid username.";
                                return View(db_table);
                            }
                        }
                    }
                    else
                    {
                        ViewBag.message = "Unsuccessful login: No roles assign to the user " + db_table.EMAIL + ".";
                        return View(db_table);
                    }
                }
                else
                {
                    ViewBag.message = "Unsuccessful login: Invalid username or password.";
                    return View(db_table);
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = DAL.LogException("Login", "Index", db_table.EMAIL, ex);
            }
            return View();
        }
        public ActionResult Logout()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();

                #region User Log

                BIO_LOGIN login = context.BIO_LOGIN.Where(m => m.USER_NAME == SessionUser).FirstOrDefault();
                user_login_logout_log Log = new user_login_logout_log();
                Log.SNO = Convert.ToInt32(context.user_login_logout_log.Max(m => (decimal?)m.SNO)) + 1;
                Log.User_Email = login.EMAIL;
                Log.Type = "LOGOUT";
                Log.Login_Time = DateTime.Now;
                Log.USER_TYPE = login.TYPE;
                Log.USER_IP = Session["IP"].ToString();
                context.user_login_logout_log.Add(Log);
                context.SaveChanges();

                #endregion

                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
            }
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ExpiresAbsolute = DateTime.UtcNow.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-Cache";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return RedirectToAction("Index", "Login");
        }
    }
}