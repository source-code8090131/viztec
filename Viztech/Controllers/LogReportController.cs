﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;

namespace Viztech.Controllers
{
    public class LogReportController : Controller
    {
        //ViztecEntities context = new ViztecEntities();
        // GET: LogReport
        #region User Activity Report
        public ActionResult LoginLog()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LogReport", "LoginLog", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            var Data = context.SP_BIO_GET_USER_LOGIN_LOGS_ADMIN(null, null, "ALL").ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("LogReport", "LoginLog", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult LoginLog(DateTime? FromDate, DateTime? ToDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LogReport", "LoginLog", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            if (FromDate != null && ToDate != null)
                            {
                                //string Fromdate = FromDate.ToString();
                                //string Todate = ToDate.ToString();
                                var Data = context.SP_BIO_GET_USER_LOGIN_LOGS_ADMIN(FromDate, ToDate, "Date").ToList();
                                if (Data.Count() > 0)
                                {
                                    return View(Data);
                                }
                                else
                                {
                                    message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                }
                            }
                            else
                            {
                                if (FromDate == null && ToDate == null)
                                    message = "Select start date and end date to continue";
                                else if (FromDate == null)
                                    message = "Select start date to continue";
                                else if (ToDate == null)
                                    message = "Select end date to continue";
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("LogReport", "LoginLog", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        #region Biometric Activity Report
        public ActionResult BioActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LogReport", "BioActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        return View();
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("LogReport", "BioActivity", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult BioActivity(DateTime? FromDate, DateTime? ToDate, string CNIC)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("LogReport", "BioActivity", Session["USER_ID"].ToString()))
                {
                    using (ViztecEntities context = new ViztecEntities())
                    {
                        string message = "";
                        try
                        {
                            if (FromDate != null && ToDate != null || CNIC != "")
                            {
                                if (FromDate != null && ToDate != null && CNIC == "")
                                {
                                    var Data = context.SP_BIO_GET_USER_DETAIL_LOGS_ADMIN(FromDate, ToDate, CNIC, "Date").ToList();
                                    if (Data.Count() > 0)
                                    {
                                        return View(Data);
                                    }
                                    else
                                    {
                                        message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy");
                                    }
                                }
                                else if (FromDate != null && ToDate != null && CNIC != "")
                                {
                                    var Data = context.SP_BIO_GET_USER_DETAIL_LOGS_ADMIN(FromDate, ToDate, CNIC, "DateandNic").ToList();
                                    if (Data.Count() > 0)
                                    {
                                        return View(Data);
                                    }
                                    else
                                    {
                                        message = "No Data found in the system : On selected Start date : " + Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " End date : " + Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " & CNIC No : " + CNIC;
                                    }
                                }
                                else if (FromDate == null && ToDate == null && CNIC != "")
                                {
                                    var Data = context.SP_BIO_GET_USER_DETAIL_LOGS_ADMIN(FromDate, ToDate, CNIC, "Nic").ToList();
                                    if (Data.Count() > 0)
                                    {
                                        return View(Data);
                                    }
                                    else
                                    {
                                        message = "No Data found in the system : On selected CNIC No : " + CNIC;
                                    }
                                }
                            }
                            else
                            {
                                if (FromDate == null && ToDate == null)
                                    message = "Select start date and end date to continue";
                                else if (FromDate == null)
                                    message = "Select start date to continue";
                                else if (ToDate == null)
                                    message = "Select end date to continue";
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("LogReport", "BioActivity", Session["USER_ID"].ToString(), ex);
                        }
                        finally
                        {
                            ViewBag.message = message;
                        }
                    }

                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion
    }
}