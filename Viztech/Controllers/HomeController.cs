﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;

namespace Viztech.Controllers
{
    public class HomeController : Controller
    {
        ViztecEntities context = new ViztecEntities();
        // GET: Home
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult Menu()
        {
            if (Session["USER_ID"] != null)
            {
                string UserID = Session["USER_ID"].ToString();
                BIO_LOGIN LoginUser = context.BIO_LOGIN.Where(m => m.USER_NAME == UserID).FirstOrDefault(); 
                string Assigned_Roles = context.BIO_LOGIN_ROLES.Where(m => m.ID == LoginUser.ID).Select(m => m.Roles_Assign).FirstOrDefault();
                string[] RoleIds = Assigned_Roles.Split(',');
                List<string> FunctionNames = new List<string>();
                foreach (var item in RoleIds)
                {
                    int RoleId = Convert.ToInt32(item);
                    string stringFunctions = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == RoleId).Select(m => m.Assign_Role_Details).FirstOrDefault();
                    string[] FNames = stringFunctions.Split(',');
                    foreach (var FName in FNames)
                    {
                        if (!FunctionNames.Contains(FName))
                            FunctionNames.Add(FName);
                    }
                }
                List<BIO_FUNCTION> menu = context.BIO_FUNCTION.Where(m => FunctionNames.Contains(m.F_NAME)).Distinct().ToList();
                return PartialView(menu);
            }
            else
            {
                return PartialView();
            }
        }

        [HttpPost]
        public JsonResult SessionTime()
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                string message = "";
                try
                {
                    BIO_SYSTEM_SETUP Entity = context.BIO_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                    if (Entity != null)
                    {
                        var coverletters = new
                        {
                            SessionTime = Entity.SS_SESSION_TIME
                        };
                        return Json(coverletters, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("Roles", "DeactivateGroup", Session["USER_ID"].ToString(), ex);
                }
                return Json(message);
            }
            else
            {
                return Json("Invalid Request");
            }
        }
    }
}