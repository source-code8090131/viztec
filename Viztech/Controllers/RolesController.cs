﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;

namespace Viztech.Controllers
{
    public class RolesController : Controller
    {
        ViztecEntities context = new ViztecEntities();
        // GET: Roles
        public ActionResult Index(int R = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Roles", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (R != 0)
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == R).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                string PreviousFunction = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == R).Select(m => m.Assign_Role_Details).FirstOrDefault();
                                string[] RoleName = PreviousFunction.Split(',');
                                List<SelectListItem> items = new SelectList(context.BIO_FUNCTION.OrderBy(m => m.F_PARENT_ID).ToList(), "F_NAME", "F_DISPLAY_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    if (RoleName.Contains(item.Value))
                                        item.Selected = true;
                                }
                                entity.FunctionsList = items;
                                return View(entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + R;
                                return View();
                            }
                        }
                        else
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = new BIO_ROLES();
                            entity.Entity.BIO_ROLE_ID = 0;
                            List<SelectListItem> items = new SelectList(context.BIO_FUNCTION.OrderBy(m => m.F_PARENT_ID).ToList(), "F_NAME", "F_DISPLAY_NAME", 0).ToList();
                            entity.FunctionsList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Roles", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(AssignFunctions_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Roles", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    string NewRoleName = "";
                    try
                    {
                        var SelectedFunctions = String.Join(",", db_table.SelectedFunctions);

                        BIO_ROLES MasterEntityToUpdate = new BIO_ROLES();
                        MasterEntityToUpdate = context.BIO_ROLES.Where(m => m.Role_Name.ToLower() == db_table.Entity.Role_Name.ToLower() && m.BIO_ROLE_ID != db_table.Entity.BIO_ROLE_ID).FirstOrDefault();
                        if (MasterEntityToUpdate == null)
                        {
                            MasterEntityToUpdate = new BIO_ROLES();
                            if (db_table.Entity.BIO_ROLE_ID != 0)
                            {
                                MasterEntityToUpdate = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == db_table.Entity.BIO_ROLE_ID).FirstOrDefault();
                                MasterEntityToUpdate.Role_Name = db_table.Entity.Role_Name;
                                NewRoleName = MasterEntityToUpdate.Role_Name;
                                MasterEntityToUpdate.GROUP_DESCRIPTION = db_table.Entity.GROUP_DESCRIPTION;
                                MasterEntityToUpdate.Assign_Role_Details = SelectedFunctions;
                                MasterEntityToUpdate.Added_by = Session["USER_ID"].ToString();
                                MasterEntityToUpdate.Activity = "Maker";
                                if (MasterEntityToUpdate.Added_by == "Admin")
                                {
                                    MasterEntityToUpdate.GROUP_STATUS = "Active";
                                    MasterEntityToUpdate.Activator_ID = MasterEntityToUpdate.Added_by;
                                }
                                else
                                {
                                    MasterEntityToUpdate.GROUP_STATUS = "Deactive";
                                    MasterEntityToUpdate.Activator_ID = null;
                                }
                                context.Entry(MasterEntityToUpdate).State = EntityState.Modified;
                            }
                            else
                            {
                                MasterEntityToUpdate.BIO_ROLE_ID = Convert.ToInt32(context.BIO_ROLES.Max(m => (decimal?)m.BIO_ROLE_ID)) + 1;
                                MasterEntityToUpdate.Added_Date = DateTime.Now;
                                MasterEntityToUpdate.Role_Name = db_table.Entity.Role_Name;
                                NewRoleName = MasterEntityToUpdate.Role_Name;
                                MasterEntityToUpdate.GROUP_DESCRIPTION = db_table.Entity.GROUP_DESCRIPTION;
                                MasterEntityToUpdate.Assign_Role_Details = SelectedFunctions;
                                MasterEntityToUpdate.Added_by = Session["USER_ID"].ToString();
                                MasterEntityToUpdate.Activity = "Maker";
                                if (MasterEntityToUpdate.Added_by == "Admin")
                                {
                                    MasterEntityToUpdate.GROUP_STATUS = "Active";
                                    MasterEntityToUpdate.Activator_ID = MasterEntityToUpdate.Added_by;
                                }
                                else
                                {
                                    MasterEntityToUpdate.GROUP_STATUS = "Deactive";
                                    MasterEntityToUpdate.Activator_ID = null;
                                }
                                context.BIO_ROLES.Add(MasterEntityToUpdate);
                            }
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                #region Group Role Log
                                RowCount = 0;
                                BIO_ROLES_LOG_TBL GroupLog = new BIO_ROLES_LOG_TBL();
                                GroupLog.Role_Name = MasterEntityToUpdate.Role_Name;
                                GroupLog.Assign_Role_Details = MasterEntityToUpdate.Assign_Role_Details;
                                GroupLog.Role_Description = MasterEntityToUpdate.GROUP_DESCRIPTION;
                                GroupLog.ADDUSER = Session["USER_ID"].ToString();
                                GroupLog.IP = DAL.GetIP();
                                GroupLog.DATE_TIME = DateTime.Now;
                                GroupLog.Description = "New Role Added";
                                GroupLog.Activity = "Checker";
                                context.BIO_ROLES_LOG_TBL.Add(GroupLog);
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Data Inserted Successfully " + RowCount + " Affected";
                                }
                                #endregion
                            }
                            else
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                        else
                        {
                            message = "A Role with the given name " + db_table.Entity.Role_Name + " Already Exist";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Roles", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    string PreviousFunction = "";
                    AssignFunctions_Custom entity = new AssignFunctions_Custom();
                    entity.Entity = new BIO_ROLES();
                    entity.Entity.BIO_ROLE_ID = db_table.Entity.BIO_ROLE_ID;
                    if (db_table.Entity.BIO_ROLE_ID != 0)
                        PreviousFunction = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == db_table.Entity.BIO_ROLE_ID).Select(m => m.Assign_Role_Details).FirstOrDefault();
                    else
                    {
                        entity.Entity = context.BIO_ROLES.Where(m => m.Role_Name == NewRoleName).FirstOrDefault();
                        PreviousFunction = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == entity.Entity.BIO_ROLE_ID).Select(m => m.Assign_Role_Details).FirstOrDefault();
                    }
                    string[] RoleName = PreviousFunction.Split(',');
                    List<SelectListItem> items = new SelectList(context.BIO_FUNCTION.OrderBy(m => m.F_PARENT_ID).ToList(), "F_NAME", "F_DISPLAY_NAME", 0).ToList();
                    foreach (SelectListItem item in items)
                    {
                        if (RoleName.Contains(item.Value))
                            item.Selected = true;
                    }
                    entity.FunctionsList = items;
                    return View(entity);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult RoleList()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<BIO_ROLES> ViewData = context.BIO_ROLES.Where(m => m.GROUP_STATUS == "Active").OrderByDescending(m => m.BIO_ROLE_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }

        #region Active Group
        public ActionResult ActiveGroup()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Roles", "ActiveGroup", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<BIO_ROLES> ViewData = context.BIO_ROLES.Where(m => m.GROUP_STATUS == "Active").OrderByDescending(m => m.BIO_ROLE_ID).ToList();
                    return View(ViewData);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public JsonResult DeactivateGroupRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Roles", "ActiveGroup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        BIO_ROLES UpdateEntity = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == R_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.GROUP_STATUS = "DeActive";
                            UpdateEntity.Activator_ID = Session["USER_ID"].ToString();
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                #region Group Role Log
                                BIO_ROLES_LOG_TBL GroupLog = new BIO_ROLES_LOG_TBL();
                                GroupLog.Role_Name = UpdateEntity.Role_Name;
                                GroupLog.Assign_Role_Details = UpdateEntity.Assign_Role_Details;
                                GroupLog.Role_Description = UpdateEntity.GROUP_DESCRIPTION;
                                GroupLog.ADDUSER = Session["USER_ID"].ToString();
                                GroupLog.IP = DAL.GetIP();
                                GroupLog.DATE_TIME = DateTime.Now;
                                GroupLog.Description = "DeActive the Group & No New Change";
                                GroupLog.Activity = "Checker";
                                context.BIO_ROLES_LOG_TBL.Add(GroupLog);
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Group Role : " + UpdateEntity.Role_Name + " is Deactive";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Roles", "DeactivateGroup", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        #endregion

        #region De-active Group
        public ActionResult DeActiveGroup()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Roles", "DeActiveGroup", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<BIO_ROLES> ViewData = context.BIO_ROLES.Where(m => m.Added_by != SessionUser && m.GROUP_STATUS == "DeActive").OrderByDescending(m => m.BIO_ROLE_ID).ToList();
                    return View(ViewData);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public JsonResult ActivateGroupRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Roles", "DeActiveGroup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        BIO_ROLES UpdateEntity = context.BIO_ROLES.Where(m => m.BIO_ROLE_ID == R_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.GROUP_STATUS = "Active";
                            UpdateEntity.Activator_ID = Session["USER_ID"].ToString();
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                #region Group Role Log
                                BIO_ROLES_LOG_TBL GroupLog = new BIO_ROLES_LOG_TBL();
                                GroupLog.Role_Name = UpdateEntity.Role_Name;
                                GroupLog.Assign_Role_Details = UpdateEntity.Assign_Role_Details;
                                GroupLog.Role_Description = UpdateEntity.GROUP_DESCRIPTION;
                                GroupLog.ADDUSER = Session["USER_ID"].ToString();
                                GroupLog.IP = DAL.GetIP();
                                GroupLog.DATE_TIME = DateTime.Now;
                                GroupLog.Description = "Active the Group &  New Role Assign " + UpdateEntity.Assign_Role_Details;
                                GroupLog.Activity = "Checker";
                                context.BIO_ROLES_LOG_TBL.Add(GroupLog);
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Group Role : " + UpdateEntity.Role_Name + " is successfully Activated";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Roles", "ActivateGroup", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        #endregion
    }
}