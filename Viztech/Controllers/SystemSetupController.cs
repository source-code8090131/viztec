﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;

namespace Viztech.Controllers
{
    public class SystemSetupController : Controller
    {
        ViztecEntities context = new ViztecEntities();
        // GET: SystemSetup
        public ActionResult Index(int S = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (S != 0)
                        {
                            BIO_SYSTEM_SETUP edit = new BIO_SYSTEM_SETUP();
                            edit = context.BIO_SYSTEM_SETUP.Where(m => m.SS_ID == S).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "No data found in the system-setup.";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Users", "AddUser", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("UnAuthorizedUrl", "Error");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(BIO_SYSTEM_SETUP db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Users", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        BIO_SYSTEM_SETUP UpdateEntity = new BIO_SYSTEM_SETUP();
                        if (db_table.SS_ID != 0)
                        {
                            UpdateEntity = context.BIO_SYSTEM_SETUP.Where(m => m.SS_ID == db_table.SS_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                UpdateEntity.SS_SESSION_TIME = db_table.SS_SESSION_TIME;
                                UpdateEntity.SS_MAKER_ID = db_table.SS_MAKER_ID;
                                UpdateEntity.SS_CHECKER_ID = db_table.SS_CHECKER_ID;
                                UpdateEntity.SS_CHECKER_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                int Rowcount = context.SaveChanges();
                                if (Rowcount > 0)
                                    message = "Application session time updated sucessfully.";
                                else
                                    message = "Error while updating the record.";
                            }
                            else
                                message = "No data found in the system-setup.";
                        }
                        else
                            message = "Select the record to continue.";

                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Users", "AddUser", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                   return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult IndexList()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<BIO_SYSTEM_SETUP> ViewData = context.BIO_SYSTEM_SETUP.ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
    }
}