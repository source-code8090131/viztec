﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.Services.Description;
using System.Xml;
using Viztech.Models;
using Viztech.BioVeriSysBranchBankAccountClient;

namespace Viztech.Controllers
{
    public class VerificationController : Controller
    {
        ViztecEntities context = new ViztecEntities();
        // GET: Verification

        #region Nadra Verification
        public ActionResult Biometric()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Verification", "Biometric", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        VerificationData Entity = new VerificationData();
                        //Entity.ValidateTimer = "Timer";
                        return View(Entity);
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Verification", "Biometric", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Biometric(VerificationData Entity)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Verification", "Biometric", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (Entity != null)
                        {
                            
                            BIO_REQUEST_LOG CheckLog = new BIO_REQUEST_LOG();
                            CheckLog = context.BIO_REQUEST_LOG.Where(m => m.RL_CLIENT_NO == Entity.CitizenNumber && System.Data.Entity.DbFunctions.TruncateTime(m.RL_REQUEST_DATETIME) == System.Data.Entity.DbFunctions.TruncateTime(DateTime.Now)).OrderByDescending(m => m.RL_ID).FirstOrDefault();
                            if (CheckLog == null || CheckLog != null && CheckLog.RL_AMENDMENT_NO < 15)
                            {
                                #region Development
                                //LogVerification(Entity.CitizenNumber);
                                //Entity.response = new VerifiedClientFromNadra();
                                //Entity.response.NAME = "Test User";
                                //Entity.response.FATHER_OR_HUSBAND_NAME = "Testing Father";
                                //Entity.response.PRESENT_ADDRESS = "Present Address";
                                //Entity.response.CNIC_NO = "42202-2021525";
                                //Entity.response.PERMANANT_ADDRESS = "Permanant Address";
                                //Entity.response.DATE_OF_BIRTH = "01/01/2000";
                                //Entity.response.BIRTH_PLACE = "Karachi, Pakistan";
                                //Entity.response.EXPIRY_DATE = "01/01/2030";
                                //Entity.response.IMAGE_URL = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAxoAAAIACAYAAAAFehobAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAARdEVYdFRpdGxlAFBERiBDcmVhdG9yQV68KAAAABN0RVh0QXV0aG9yAFBERiBUb29scyBBRxvPdzAAAAAtelRYdERlc2NyaXB0aW9uAAAImcsoKSmw0tcvLy/XK0hJ0y3Jz88p1kvOzwUAbp8I8ZevLLgAAFoNSURBVHhe7d0JnM3l3//x95wZM8OYYez7vkfJUlIpbRTaNy2KUISUUiqlKEUpSiUladNPIqK0pygRRdnJPmMZg9nHzJn7XF9X3S2WWc7MnOX1fDzOf67P5zv3/b9/P5xz3t/vtYTkeAgAAAQ0d1KSsvcnKPvQQWUn7j8yPpDovHJSU+VOTpY7I80zTvOMUzyvQ55XqrIy0xSSnKScwznKUoZCM6SskGyF5vLrQ3ZIiMJyQpUdIYUpQiElQpRTOlquqCiFRUTKVTrG84pSSKmSnp+evtPz/IyJUWjZWIWWK6/Q2HL2Fev5nytt/zcD8HUEDQAA/JXnIzw7IUFZe+J1OG6Xsvfs1uHdccqK97x2eur9e22o8ISJHLf9H/JvISEuT/jwBBATPCpXVVilSgqrUlUlzLh6DYVVqKiwqtWc6wCKF0EDAAAf5j50SJlb/9DhbVuPvLZvVcbWzcreGaeMhDi5MrPtb+Lv3BFhTviIqFJTJWp6XnXqqUTt2gqvWdvzsw5PRoAiQNAAAKC4ZWcrY/NGZa5fp8xNGzzBYrMytm1R9mZPoEhOtr8ErypXXpG1PMGjVh0neIQ3bKTweg08r/oKCQ2zvwSgIAgaAAAUFU+gyDRPJNatUeaG9c4rbcNaZW/aIrMCAsXPTM0Kr19f4U2aKqJBE4U3auz52UglPKHEc9H+FoDcIGgAAFAYzFMKT6BI/22l0n9fqYyVvyp101q5MggU/iikZElFNG6qyGbNFdn8FEWc7HnVayCFhtrfAPBvBA0AAAooJztLmevW/jNUbFzD+okA544soVJNWhA+gGMgaAAAkEfulGSl/7JCqUt/UPrPPytt1QrlpKXZqwhmrogoRbZprZKt2qhkm9MUecqpcpUsZa8CwYWgAQDACWTF7VLa8mVKW7ZEqSs8wWLdGoW6A2O7WBSuEHN6SLMmKtn2dEW2busJIG0VVrGSvQoENoIGAAD/kr1vn1KXLFbqooVKXbxIh+N22CtAwYXWqqnS7TuoVPuzVardmQotU9ZeAQILQQMAEPTMVKjUHxcr7c9wsWljrk++BgrCnJxeskkzRbU/S1FnnqOSrdsqJDLSXgX8G0EDABB8PB99ZuF2yrdfKfX7hUpf8Qvby8InmO11I1q3VumzOirqnI6KaNbcXgH8D0EDABAU3GmpSln4tVK//Vop33yhrIQEewXwXa4qVRV97nmK6uB5ndmBpx3wKwQNAEDAOrx1i5K//coTLL5U2uLvlZPDAm74L7OdbunTzlbpjud7XhcorGo1ewXwTQQNAEBAydiwTsmffKyDn36srE0bbRcIPOFNT1LMRZeodOdLFG7O7wB8DEEDAOD3Mlb/piRPuEj+9BNlbttsu0DwCKvfQGU6d1Xpi7sqomFj2wWKF0EDAOB/PB9d6b+uUNJn85U8fx7bzwJ/E16rnkp3vljRJnSwmBzFiKABAPAbmVs269DsD5Q0exbhAsgFEzqiL7tcMZdfrRI1atouUDQIGgAAn2YOzzv08SwlffSh0lf/ZrsA8iqyZSvFXHaVortcyiGBKBIEDQCAz8lJT1fS55/o0KwPlPTD9wp1s1sU4C3mrI6ojucr+vKrVLrjhQoJD7dXAO8iaAAAfEb6L8t18H/vKunjj+XOSLFdAIUmOkplulyustfdxHoOeB1BAwBQrLIPHtChjz7UofffVsbGDbYLoKiZ7XLLXnuDYrpdIVd0tO0C+UfQAAAUPc9HT+qSxTo4410d/GyeXJnZ9gKA4hZSsqSiO3dRmau7q2Sb02wXyDuCBgCgyGQn7veEi+lKnPG2srdtt10AvsqczxF77Q0qc+V1csXE2C6QOwQNAEChMwfqJU57XQfmz5YrI8t2AfgLV0SUoq+4XGVv6smBgMg1ggYAoFDkZGcp+YsFOvDGa0pbscx2Afi7Uqe1V9lbb1Pp8y70fJMMsV3gvwgaAACvcqZHTX9H+6e/JXd8nO0CCDQlqtZQmRtuVtnrb2JaFY6KoAEA8IrMzRuV+NorSvx4JtOjgCBiFo/HXHalyvW6QyVq17FdgKABACggc/bF/kkv6uDXXyiUjxQgaGW7XCpzQWeV69tfkS1OsV0EM4IGACDvPB8dyV99rsTJL7P+AsB/mHUcsX3uUFSHjraDYETQAADkWs7hw0qaM0v7Xn9ZWZs22i4AHF1Eo6aK7dtP0Zd0U0homO0iWBA0AAAnlJOZqYPT39be115Uzu69tgsAuWMWjpe7vb9irr5eISVK2C4CHUEDAHBMJmAc8ASMxFcmKCshwXYBIH9cVaqq4h0DCRxBgqABAPgPAgaAwmQCR4XbB6jMNd0JHAGMoAEA+IsTMN59U4mvTiRgACh0fwUO84QjPNx2ESgIGgAAZ5H3gffe4gkGgGIRUrmiKvW7WzHXdmfReAAhaABAMPN8BBz6eLYSnh2jw3E7bBMAikdorZqqNPgBZ5cqhYTYLvwVQQMAglTKwq+1b+xoZaxfYzsA4BsimzVXhXsfVKkzz7Yd+COCBgAEGXOS956xo5S+bKntAIBvMgf/VRj6ICeN+ymCBgAEiYwN65Qw7mnnRG8A8CelL7pEFYbcr/A69WwH/oCgAQABLvvgASU8P1YJ099WqNttuwDgX0JCXCrbo6fKD7hHrpgY24UvI2gAQIDKyc7SgXemaf/4scpOTrZdAPBvodGxKn/PvSp73Y2eItR24YsIGgAQgMxC770jRyhz22bbAYDAEtGgoSo++BgLxn0YQQMAAkjm5o3aM+pRpS5aaDsAENhKn3ehKj7wiErUrmM78BUEDQAIAO6kJCW88KwOvPmmcpRluwAQHJz1G736qPydg+WKKm27KG4EDQDwc0nz5mjvE49wojeAoGdOGK/y0EhFd+piOyhOBA0A8FOZWzZrz+PDmSYFAP9S6swOqvzoE0ynKmYEDQDwMzmZmUp46Xnte/0luTKzbRcA8HfuiDBV7DtI5freqZDwcNtFUSJoAIAfMbtJxY98SNnbttsOAOB4wmvVU6URo9idqhgQNADAD2Tv26c9I4cr6dOPbQcAkBfRXS9VpWGPKbRCBdtBYSNoAICPOzT7A+31hAwO3QOAgjGH/VV89DHFdLvCdlCYCBoA4KOy4nYp/pFhSl34le0AALzBnL1R6fGnFFaxku2gMBA0AMDXeN6WD05/W3tHPyF3RoptAgC8KjpKlYc+ojLX3mAb8DaCBgD4kMNbt2j3w/cr9afFtgMAKEylTmuvyqOfUYkaNW0H3kLQAABf4HkrTnzzNe17fqxy0tJsEwBQFFwRUapw71CVvbmn59txiO2ioAgaAFDMnLUYQ+/mKQYAFDPzdKPKmOcUVrWa7aAgCBoAUIwOzZ2l+MeGSUmsxYBvyw4JUUhEmEqERXh+Rjg9d4lwhYaEen6GKFQlnF62Dst1OEfZOdmen5lOLycjQ4cPp8uVkeXUgE+LjlLVx8YousultoH8ImgAQDFwHzqk3cPv51wMFCkzPSSkcjmVKFdRYeXKK7RSJYXGlJUrOkahZT0/y5RRaOkYuWI8r+hoz+9HKqRUqSM/Tbgo6JQSz1cOEzrcaanKSU93frpTUpx/D9kHEj0/Dyo7KUnuJE996ICy9+xR1v4EZe/eo4zE3XKlH7b/i4DCZ87dqPzoaOffA/KHoAEARSx10XfaOewu5ezeaztAwZkQUaJmNYVWq6kS1aqqRJXqCqvq+VmjlrOFpwkVrpKl7G/7J3dKsnN45eG4ncqKj1PWrl06vNvz2r5dmfG7lLlzK2EEXuWqUlXVnn5OpdqdaTvIC4IGABSRnMxM7R39mBLee0uhvPUiH9wRYYqs11gRteuqRN26Cq9ZRyVqH3lxHoCH599V1r69zu5tmZs3egLINmVu+UOHN29Q5qZNnstu+4tA7plpgxV69FKFex9USHi47SI3CBoAUAQyNqxT/OB+yti4wXaAYwtRmMIbNXReEQ2aeH56wkWDRke23wwNtb+FvMjJznICiPm3eNgTOjLWrVHmxnVK3bxJoW4CCE4sokFDVX3hVYXXa2A7OBGCBgAUsoMz39eekcPZthZH5Y4sochGTVWyWQtFnnSyIpqepIjGTblzWkTMWpGMtauVseZ3pa/9XRkrf1XqxjVyZWbb3wD+X0jJkqo8fJRirrrWdnA8BA0AKCRmoeueR4bp0JwPbQeQwmvVU2TLloo8tbVKtmyl8MZNFBIaZq/CF+QcPuyEj/QVPyvt15+V/vNyHY7bYa8CUsylV6rS46P9ft1TYSNoAEAhyFj9m+LuulOZ2zbbDoJRSIhLEae0VKnTzlDJtqcr8pRTFVqmrL0Kf2IWoaf9ulxpP/+ktB8WKcUTRJhyFdzMTYOq4ycqollz28G/ETQAwMsOvDtN8aMfYepFEHKCRevWKtW2neflCRet2yokMtJeRSAxO2Cl/bxUaUuXKPWnH5Txy0rliHNCgo07PFRVhj2usjf0sB38HUEDALzEfPGIf2CIkj+bbzsIBuauZlTHjip11jnOUwumUgQnd1KSEzhSFn6l5MULlb1tu72CYFD6oktU5aln5YoqbTswCBoA4AVmK81dt9/GVKkgYM6rKHXOOYo69zxFtT9bYVWr2SvA/zu8Y7tSv/vGEzy+VsoP37MZRBBwplK99KoiGja2HRA0AKCAkhbM0+77hsidkWI7CDQlqtZQVMfzVPqCzip5WjuFlChhrwAnZna2Mk87kr9coENff8ZhnQHM3IioPPZZRXfqYjvBjaABAPmVna29z45W4uuTbAOBJLzpSYq56BJFXdiJO5TwKrNZRPIXC3Tw04+VtWmj7SKQxN52uyoOGRb0594QNAAgH7IT9ytuUD+l/rTYdhAIIps1V+mLuym60yXOadtAYTMHCCYvmK/kT+ZyoGeAKXVae1Wd8LJCY8vZTvAhaABAHpm7kbv692Ff/QAR0aiportdruhLuh05eRsoJplbNit5/sc6NGsm670ChKtKVdV4cbIiW5xiO8GFoAEAeXBo9gfa9ch9cmWwjaU/Mx/+ZS+9UtGXXsG0KPik9FW/KmnuLCV9PFtZCQm2C39ktsCtOuJplbnqOtsJHgQNAMgNz1vlvnFPa/+rE20D/sYs0ozu2lUxV17rnG+hkBB7BfBh2dlK/XGxc5PjwGdz5Uo/bC/A3zjrNu57KKjeewgaAHAC7rRUxd8zQMlffW478CclT22jMtd0V+lLunLGBfyaOavDPOE4+L93lb76N9uFPyl93oWq8syEoDlvg6ABAMeRFbdLO/r1Uuaa320H/iCsfHlFX361ylx7g8Lr1LNdIHCYReSHZrynQzPfV3Zysu3CH0Q0aKjqr70dFGfwEDQA4BjMHOldt9/C/Gg/Yp5elL25p6I7dwn6bSURHHIyM5U0d7YOvP0GTzn8SbnyqvXyFEW2bGUbgYmgAQBHkTRvjnY+OJj50H7AWXtxxeUqe1NPFnYjqJmbIwfemaoDn3zEe5cfMIvEq418RjGXX207gYegAQD/sv+lCdo94RmF8vbo00Jr1VT5m3sr5qprg2a+M5Ab2QcP6OD772q/J3S44+NsF76qXL+BqnDXfZ5v5YG3SJygAQB/ys7W7uH36+DM920DviiyTVuV63WHs6iSnaOAY8vJzlLyp/OV+NrLTKvycTGXXqkqT40LuCmfBA0A8MhJT9fOgbcrdeFXtgNfEqIwle56iWJv7Ru0B18BBZG27Cclvvm6Dn7xqULdbtuFLyl1ZgdVe2FSQD2hJWgACHrZifu1ve/Nyly50nbgK9wRYSp31Q0q1/fOoNihBShs5vTxxFdf0qEPZygnh8Dha8KbnqQak6cprGIl2/FvBA0AQe3w1i3a1vsGZW/bbjvwCdFRKtf9VsXe0luhFSrYJgBvMVt3J77xqg78713lpKXZLnxBiao1VH3KWwqv18B2/BdBA0DQMju0bOt7i7Sf7Wt9RWh0rGJv66OyN90qV3S07QIoLOaJbuKbr2n/21OkpBTbRXELLV1a1Sa9qZJtTrMd/0TQABCUUhZ+rbgB/eTO4IPVF5gP1dje/VW2R092kAKKgfvQIe2f8ooOTJnC+6KPMFNHq499UdGdutiO/yFoAAg65oyM+CEDmZ/sC6KjVKGXJ2Dc0ouAAfgAszVu4muewPHmVAKHD8h2uVR91FhnG29/RNAAEFTM1rW7Hh7KrivFzByyF9u7j2J79mWKFOCDzJSq/ZNfUsLbr8uVkWW7KA7ZISGq+uAIle3Ry3b8B0EDQNBInDpZ8U+N5CC+YhQS4lKZ7jep/KAhCo0tZ7sAfJVZNJ7wwjjtnzWDGzTFzBzqV67/IFv5B4IGgKCQ8OJzzoclioe5I1e2SzdVuPsBlahR03YB+IvMzRu1b+yTSv7qc9tBcYi97XZVHPqwrXwfQQNAwNs7ZpQSX59kKxQ1cwhVxXuHKaJZc9sB4K/Sf1muvU+NVNqKZbaDolbm+htVecRo84jYdnwXQQNA4PK8ve0eMUwHp79jGyhK4bXqqcIDD6n0+RfZDoBAYTbV2Dd2tA7H7bAdFKXorpeqypjxCgkNsx3fRNAAEJg8b23xQwfr0JwPbQNFxWxVW27gPYq9uZenCLVdAIEmJzPTeVq8/+WJ7FBVDEqfd6GqvTjZp99nCRoAAg8ho1iYbRjLX3+Tyg++T6FlytougECXtXeP9j07WomzZ7LZRhHz9bBB0AAQWAgZxaLkqW1UaeRTimjY2HaAgss8nK0XZ36nEqEuDbymw5EmfJZZvxE/4kFlrvnddlAUoi7spOrjJ/lk2HDZnwDg/0zIGHYPIaMolSuvKk8/p5rvfUjIgNe43Tl6Y94SNbj2cQ158SMlJKXaK/BlkS1bqc7Mear40GPOYZwoGimfL9DOu26XsrNtx3cQNAAEhj9DxqwPbAOFyUyTKntDDzVYsFAxl1/tF7ufwD989N0qNb/5afUaPV3b9x6yXfiN0FDF9uil+p9+r5grPO8NKBImbOwa3F852b51uCJBA4D/84SM3Q/dR8goIuFNT1Ld/81RpUefkCsmxnaBgln4yya1v2O8Lh82RWu27rZd+KvQChVU5annVPOdmc4OdCh8yZ/NV9zgO30qbBA0APg3GzIOznzfNlBY3JElVGHIMNWe+bEiW5xiu0DBrNoUp65DX9U5A17UD79tsV0EipJtTlPteZ+rXL+BCgnha2dh87WwwZ84AL/mnJNByCh0pU5rr/pzvlK5vv19ft92+IctcfvVY+Q7atnzKc1bvMZ2EYhCwsNVYfBQ1Zr7mbOOA4XLhI34oXc5N+KKG0EDgN/a9+xTHMZX2KKjVPnJZ1TjrfdVonYd2wTyb++BZA0eP0uNbxiptxYsk9vNV5FgYTaMqDV9trNY3BXBYvHClPTxHOdGXHHjXzcAv7T/1Zc8r4m2QmEo1eE81ftkocpcdZ3tAPmXlJqhkW8sUP1rH9P4GQuVedheQHAJCXEWi9ee/7ki27S1TRQGcyNu75hRtioeBA0Afufg/951DodCIfnzKcbkNxVWsZJtAvljzsJ4wRMsGlw3Uo+8/qkncPjWrjgoHiVq1FStt2c6TzfM+i8UjiMnt79gq6JH0ADgV5LmzdGuR4v/cXCgKnVmB9Wb+xVPMVBg5jzgtxcsU5PuT2rQ+Fnak5hirwCWfbph1n+xdqPw7Ht+jA68O81WRYugAcBvpCz8WvFDBirU7bYdeIu5o2i2q63x+tsKq1rNdoH8+fTHtWrV61ndPPId/RG/33aBozPrv8zaDbOrHTtTFY64xx/WodlFvwU8f5oA/ELasp+0feBtyskhZHibORej3qzPnAP4zB1GIL+WrN6qjgMm6uJ7J+mXDTttF8gFz3uP2dWu1ofzOHejEISareDvv0/JX35mO0WDoAHA52VsWKftd/SQK53Vo96UbaYt3Ha7as+Yq/B6DWwXyLt12/boygenqF3f5/XNLxttF8i7iGbNVXvuApW5/kbbgbfkKEs7Bvd1btwVFYIGAJ+WtXePtve+WUpifrc3hVSuqNpT31PFoQ8rpAQLMZE/O/YcUO/R7+uk7k9r1sJVtgsUTEhkpCo/9pSqvfS6QqNjbRfe4MrM1q7bb1Hm5qK5IUDQAOCz3CnJ2tGnh9zxcbYDbyh93oXOgu9S7c60HSBvDiSn6b6Jc9Tg6lF6fd6Pyg5hSiO8r/T5F6n2J184B4bCe7KTk7Wz183K3rfPdgoPQQOAb8rO1q6Btytzze+2gYIyiywrDntE1V6eotAyZW0XyL3U9Ew9/c6XqnvFSD3z3tfKcGfbK0DhMFts15g2XeXvHKxsF19bveVw3A7t7HOz3GmptlM4+BMD4JPiH7pXqYsW2goFVaJqDdWcPkuxt/axHSD3srLdenXOYjXqPlIPvPyxDqSl2StAEQgJUflBQ1T7jXcVVr68baKg0lf/pl139lFOduGdbUPQAOBzzJ7fh2YV/TZ8gcpMlao9ZwH71CNfPvj6VzW/+WndPmaGdu5Ntl2g6JnpnrXnMJXKm8wNvT3DH7CV9xE0APgUc+p3cZ5iGkjMNAOzL72ZKuWKibFdIHe+WbFRp/UZp2uGT3V2lQJ8QWiFCs5UqnL9Bjo756HgDs58XwkTnrWVdxE0APiM1EXfac/wh2yFAilX3plmYPalB/Ji+fod6jxkkjoOnKila7bbLuBDPAGjwuChqjnxNSk6yjZREAkTny+UA/0IGgB8wuGtWxQ3qK+zzzcKJrJZc9X7cD67SiFPNu3cpxtGTFOb28ZpwZK1tgv4LrMrVZ0P5iusPucAecOu4fcq/ZfltvIOggaAYuc+dEg7e9/ibLmHgilz1XWq+f5HCqtazXaA44vfn6QBz36gpjc+pfe+WKGcnBx7BfB94XXqqc7MeYru3NV2kF/OGRv9eykrbpftFBxBA0DxMtvYDu6nzG2bbQP5YbaurTziSVV+8hmFhIfbLnBsSakZGj55vhpc85gmzlqkw1lsVQv/5CpZSlXHv+ysSWML3ILJSkjQjn69vLbtLX8aAIrV3qdHso1tAZmTc6tPfVdlut9sO8CxZR7O1rjp36je1SM06s3PlZJBwEBgMGvSar44Wa4I1m0UhDm/Kn7o3ZIXnm4SNAAUG7PTReKbr9sK+WHmJteaPY/1GDghtztH0z5dqkbdn9CQFz/SvkPp9goQOMy6jZozP3LODkL+JX82XwkvjLNV/hE0ABSLtGU/ac9DQ22F/CjV4TzVmTFXJWrUtB3g6OYtXq2Wt47RLaPe1db4RNsFAlNEw8aqNWueItu0tR3kh9mJKmneHFvlD0EDQJEzC822D+yrnBy37SCvYm+5TTVenSpXVGnbAf5r8ao/dFa/8eo6dLJWbY63XSDwhcaWU61pM5wNMpB/Ox8crIzVv9kq7wgaAIpUzuHD2jGoj7Q/wXaQF2ahY6VHn1DFB0c4e8kDR7N6S7y63TdZZ/aboEWrttguEGRCQ50NMircdZ9tIK9c6Ye1q38fZR88YDt5Q9AAUKT2jBquzJUrbYW8MAsczULHsjf0sB3gn7btTlTPJ95Vix6j9fEPq20XCG7l+g9SlWcmOLvzIe8Ox+1Q3L135WtxOP+NAygyh+bO0sHp79gKeRFWvrxqTv/AWegI/FvCwRTd88JsNbrmCU39ZKncbj7egb+L6XaFszsfJ4nnT+rCr5w1G3nFOxGAIpGxYZ12D7/fVsgLZ2epmfMV0ay57QBHpKZnauQbC1Tv2sf13PvfKsPNVrXAsZjd+er872N2pMqnPZ6gkbLwa1vlDkEDQKFzpyQrrn9f5aSl2Q5yq+SpbVRnOid945/MWRgvz1qketeM0iOvf6pDKZn2CoDjCa/XQDVnfKSIBg1tB7kV6nYr/u7+Orxju+2cGEEDQKGLu38wJ3/ng9m+tsbU9+SKibEdBLucnBxN/2KFmt30lPo/+4F2JybZKwByK6xiJdV8b7ZzIwd5k52crDiza2Rm7m5uEDQAFKr9r76klM8X2Aq5FXPplar+yusKiYy0HQS7z5euU5vbnlH3EdO0aec+2wWQH+YGjrmRU/q8C20HuZW++jftGfGgrY6PoAGg0KT/slwJz461FXLLnJFRZczzCgkNsx1A6vnk21q+fpetABSUuZFT9cVXFXPF1baD3Do4830dmv2BrY6NoAGgULiTkhQ3+E7lKMt2kBsVBg/ljAwAKCLmhk6V0eOcGzzImz2PPKzDW49/Tg9BA0Ch2P3wUGfvbeRexWGPqFy/gbYCABSJkBDnBk/5OwfbBnLDnZGiuMH9nIN4j4WgAcDrzCPVpE8/thVOJNvzIVd5xJOKvbWP7QAAilr5QUNUYcgwWyE3zHqNfWOfsNV/ETQAeFXm5o3aM3K4rXAi2S6Xqj81TmW632w7AIDiUq5vf1V66DHnBhByZ9+0Kcc8X4OgAcBrzHZ3u+4ZwHkZuRSiMNV4/iXFXM5CRADwFWV79FK1x59ybgThxEJzcrTz/ruVve+/u+Hx3yAAr9n3zJPKXPO7rXA8ISEuVX1pkqI7dbEdAICvKHPtDao++lnCRm7tT1Dc0LvMYT+2cQT/7QHwCvPYNPHN122F4zFPMqqMf0mlz7/IdgAAvsY8ba4+aizTqHIpddFC7Z/8sq2OIGgAKDDzuNQ8NsWJOSFjwos8yQAAPxBz1bWq9gRhI7cSxj2tjA3rbEXQAOAFccOGOI9NcXzmETwhAwD8S5mrrjuyZoOwcUI5OW5ne/s/p1ARNAAUyIF3pyl14Ve2wrGYkFHjmRcIGQDgh8yajWqPPkHYyIX0X5Yr8c3XnDFBA0C+ma1s458eYSscT5X7H1Z0l0ttBQDwN2Yb8kr977IVjmff82N1eMd2heR42B4A5InZzjYnPd1WOB5XTIwdAflT44rh2rk32VbB5dFenTSiV2dbAcXI87XZnZRkCxxPSMmSBA0AAPwBQYOgAfgbpk4BAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8DqCBgAAAACvI2gAAAAA8LqQHA87BgokKTVDu/Yd1O79Sc444VCKDial6UBymhLN61CqDqakK8nzysrOUUZWludntg5nuXU487AyPeNMz9jt+RkWGqbwcM8rzKXIiBKeOlQRYWGKiAhTCZdLsTGlVM7zKu95VShbWhU9L1ObnxXKRjk/w0LJ0QACR40rhmvn3mRbBZdHe3XSiF6dbQXAXxA0kCuZh7P1R1yCNu7Yp227E53Xzr0HPcHikHbsPeD5ud8TLrLsbxe/MOWoZpXyqlOtnOpVraB61curXrXyqmtqz08TRADAnxA0CBqAvyFo4B/i9ydp7dbdWr99j+fnHufn+m179IcnYGQpxP6W/ytfJkon16+qlg2rO69TG9ZQ0zqVeQoCwGcRNAgagL8haASxLXH7tWLDTi1ft10/r92u5et3andikr0afCJcoTqpQWW1alxTZ59SXx08rzpVy9mrAFC8CBoEDcDfEDSChFknsWjlH/p+5WYtXbPdEzC2av+hDHsVx1KrQqzOaV1f55xaX+ee2kD1q1ewVwCgaBE0CBqAvyFoBKi9B5L17YpNTrD49pdNWrlpu9xupgUVVLUKZXR+64a64pyT1em0xioVGW6vwNe9PGuRHnhljq2C08KJd+mUBtVsFZgmfvi9Pl+6zlaB5dNFa5ThzrZVcGlcq5Ka1K5kq+B1z3XnqkPL+rYKXpcMmaRFv222VXDa+P5wv1hvStAIEKnpmfp6+UZ98sNqffHzBq3btsdeQWExU60uaNdIl57V3PNqoSrlou0V+KLn//et7p4w21bBaelr96hNk5q2Cky9R7+v1+f9aCsgsLw1/Ebd1KmNrYLXWf3Ga9GqLbYKTnFzHveL7x3c4vZjZo2FuXtnkn2FLsPUdehkTZy1iJBRRMydxXmL1+j2MTNU7bJH1a73cxr77lfaseeA/Q0AAIDgRdDwI+bh0+JVf+i+iXPU5IbRqnvNSA0YN1OfLFmrtAy3/S0UB/Nns2TtNg19aa5qX/2oLrzrJU3/YoXzpAkAACAYETT8wPL1O5xwUeeKkTqz3wQ9897XPLXwYWYtjJm+1n3ENFW9bLj6PD3dCYgAAADBhKDho1ZtitPwyfPV6PpRat3rWSdcbNuXaK/CXxxKydRrc5c4AdH8WZp1AubUdAAAgEBH0PAhCQdTnC+iJ/d4WiffMkaj3vxcG3Yk2Kvwd+bP0ixGrnHFI7rnhdnO6eoAAACBiqBRzMzc/q9+3qAbRkxT9W6POl9EV22Ot1cRiMxTjufe/1b1rhylax5+Q0tWb7VXAAAAAgdBo5jE70/S6Le+UMPrn9T5d72k975YEbT7ower7BC3Pvhmpdr1fV7t7xiveYtX2ysAAAD+j6BRxH7duEs9Rr6jWlc+pgcnzdOmnfvsFQSzH37b4mxPbAKHecIFAADg7wgaRcBMjzJ3q88bNFEtbx2rtxYs0+Esnl7gv0zgME+4zN8VdqoCAAD+jKBRiNIzs/TqnMU66aannbvV5uRuIDfM3xWzU5U5jHHZ2u22CwAA4D8IGoXAHNJmToiufdUI59ToNVt32ytA3pjDGE/r85wz3c6s6wEAAPAXBA0vMk8wzPa09a4Z5ZwQvScxxV4B8s9MvTPT7Rp3H6Vx079R5mGm3QEAAN9H0PAC88Vv4offq/61o5ztaXcncucZ3me2xR3y4kfOGSufL11nuwAAAL6JoFEAbneOswajwbWPa8C4mdq176C9AhSeddv26KK7X9GVD07h0D8AAOCzCBr5ZO4on3zzU84ajO17D9kuUHRmLVylZjc84TxNM9OrAAAAfAlBI4/M3eSuQ1917ij/vnWP7QLFIyUj23maZrbE3RK333YBAACKH0Ejlw4kp2nw+Flq0WOM5i1eY7uAbzDb4ba4cYxenrWIpxsAAMAnEDROwHxpc9ZhXPeExs9YyEF78FnJmRnq/+wHunDwK6zdAAAAxY6gcRxmmtS5A1501mEkHGSrWviHL39er+Y3P6m3FyyzHQAAgKJH0DgKs13tiCmf6pSbxmjhr5ttF/AfSalZunnkO+rz9HTnfBcAAICiRtD4l8Wr/lDLW8fqsSkLlOFmmhT822tzl+i03s86T+cAAACKEkHDSk3P1IBnP9BZ/V/Qmq27bRfwf6s2x6vNreM0/YsVtgMAAFD4CBoey9fvUKtez2oiO/YgQJmF4t1HTFO/sTOYSgUAAIpEUAcNc7L36Le+ULu+zzO1BEHhlY8Wq8OdExSXwCGTAACgcAVt0Nix54A6DnxRD06ax5a1CCpL12xX295jnSd5AAAAhSUog8YHX//qHG7GjlIIVjv3Juvs/i9o1sJVtgMAAOBdQRU0zLa1ZsH3NcOn6kBamu0CwclsgHDVQ2/oyWlf2A4AAID3BE3QMHPSzx34orPgG8ARZvODh16dpx4j33GCOAAAgLcERdAwZ2O0vnWMfvhti+0A+Lu3FizTeYNe1IFknvQBAADvCPigMfHD73XuwImKS0yxHQBHs2jVFnXoP0Hx+5NsBwAAIP8CNmiYaSA9n3hXA8bNZFcpIJfM4X4d+o/Xtt2JtgMAAJA/ARk0zPSPCwe/pKmfLLUdALm1YUeC2t8xTqu3xNsOAABA3gVc0NgSt985gI+ta4H8M9vfdrjzRS1bu912AAAA8iaggoY5gIxTvgHvSDiYovMGTdA3KzbaDgAAQO4FTNCYt3i1OvR7XrsTWcgKeEtSapYuufdlffXzBtsBAADInYAIGq9//KMuu+91pWSw6BvwtrQMt7rd/5oW/rLJdgAAAE7M74PG2He/Uu+n3ld2iNt2AHibOUW869BXtGT1VtsBAAA4Pr8OGsMnz9fQl+baCkBhMtOoLrr7JRaIAwCAXPHboHHPC7M16s3PbQWgKBxKydSFA1/Wrxt32Q4AAMDR+WXQGDx+lp57/1tbAShKB9LSdMGgF7RqU5ztAAAA/JffBQ0TMsbPWGgrAMVh36F0XTj4ZefcGgAAgKPxq6BhpksRMgDfYLaS7jxkknPeBgAAwL/5TdAYMeVTpksBPsYcjmm2vk3PzLIdAACAI/wiaIyb/o0em7LAVgB8yQ+/bdENI6bJ7c6xHQAAAD8IGuYwviEvfmQrAL5o1sJVGvTcTFsBAAD4eNCYt3i1bh89w1YAfNnEWYv09Dtf2goAAAQ7nw0a5gTiax95kxO/AT8y7JV5mv7FClsBAIBg5pNBY9POfep67ytKTc+0HQD+ICcnR71Gv63VW+JtBwAABCufCxoHktPU5b7Jzj79APxPg+qVVKdKOVsBAIBg5VNBIyvbrasfmupsmQnA/1SMLq2Px/ZVqchw2wEAAMHKp4LGwHEz9eXP620FwJ+El5A+fLqnalWOtR0AABDMfCZovDpnsV75aLGtAPibV+69XmedXM9WAAAg2PlE0Fi86g8NfI5tbAF/ddc1HdSzy+m2AgAA8IGgEb8/SVc/+LoyD9sGAL9y8elNNG7g5bYCAAA4oliDhtudo+6PTFNcYortINCFhIQoJipctSrEqmGN8mpau7JaNqyutk1r6ozmdZyfLepVUeNalVS7Sqwqx0Y7c//hm8yf07uP9ZDLFWI7AAAAR4TkmI3vi8nwyfM16s3PbQV/ZwJEg+oVPAGhvLMg2LzqVivvCQulVb5MlMrFlFL5mKh8fSk12x7v3p+kvQeSPa8U/bErQRu279WGHfu0cedebd+T4AmuPrW3QcArW7Kklk29R/U9f+b+4Pn/fau7J8y2VXBa+to9atOkpq0CU+/R7+v1eT/aCggsbw2/UTd1amOr4HVWv/FatGqLrYJT3JzHVaVctK18V7EFjc+XrlPnIS/x5dAPuVxunVSnmlo3rqlmdauoRb1qal6vimpUKmt/o+ilZ2bpt81xWr5+u5au2aZla3fo9z/idTgr2/4GvClMOfrk+f66oE0j2/F9BA2CBuDvCBpHEDQIGsdl7kq3uHmMdicm2Q58mXlS0eHkBjqjRV21O6m22jatpehSEfaq78o8nK0ff9/ihNqvl2/QT2u2Ezy8ZMJdV2jgNR1s5R8IGsERNMZN/0bzf1hjq8Dy/YpNynAH53tY3SrlVM9Pnp4Wpgd7XKDzWje0VfAiaBA0jqvbfZP18Q+rbQVfE5rj0mktaumi0xrroraNPeGiTkDMwU9Nz9TCXzbr40W/afb3v2rn3mR7BXlxx2Xt9fJ919jKfxA0giNoBLIaVwwP2vetR3t10ohenW2FYEfQ8J+gUeTzll6etYiQ4YMiXKHqekYzvfnwDdq3YJQWv3KX86bevkXdgFnoa06r7tyuiV4ccrW2f/i4fnx1sB646XxnUTpyp8Mp9TR+8JW2AgAAOLYiDRqbdu7TvRPn2ArFzcyzN+HCzPnc++kTmju2j3p0bquypUva3whcZver05vV1ug7umr99Ied0GHu1JsFzjg6M3Xhwyd7KbxEqO0AAAAcW5EFDbOV7c0j33Gmr6B4mTv4T/Xrqu1zRjrhwiws84c1F4XJhA4zHWj3/JF6b0QPdTq9iRNGcER0qTB9/EwfZ/cwAACA3CiyoPHs9K/1w2/BPZ+uOJl1F1efe7K+fXGAcwf//hvP94u5fUXN3K2//oJT9emzt2vNOw/ozivOVFREcN/BN7uMvTeip5rVqWI7AAAAJ1YkQWPdtj0a/sp8W6EomR2j7r7uHG3+8GHNGNVTHVrWt1dwIuYwOrOeY8dHIzWmfzfnkMFg9OTtl6pL+2a2AgAAyJ1CDxpmU6u+T78ftFvyFZcKMZHOl+Mdsx7XuIGXO4fnIX/MmpX7bjhPG2Y8pElDr3FOLA8WN3dq4zz9AgAAyKtCDxpT5i3Rwl832wqFrVxMhJ68vYs2fzDC+XIc7GsvvMlMq+p7aXutf+8hvfbAdc7i6EB2epNanv+c19sKAAAgbwo1aJiD+e4dzy5TRcGsIzD7jG/54DENu/kCAkYhMoHjtq7ttP79I084KscG3lqXahXKaPaY3uwwBQAA8q1Qg8aQFz7SgbQ0W6EwmJ2RzPSWte897Jx7QcAoOmGhLucJxwZP4DDncZizSAKBOW9kridksFkAAAAoiEILGotX/aG3P/vZVigMZmrLD5Pu0rThN6pGpbK2i6Jmwp1zHseMh9T9glP9eltc83/7mw/doFaNatgOAABA/hRK0DBnZtw5bqazEBzeZ840ePGeq/TD5MHO+Q/wDWbB/bsjeuibF+5U09qVbde/PNLzIl3d8RRbAQAA5F+hBI1JHy3WLxt22gredPHpTfTbWw/qzivP4kA5H2W2EP5l6n16/LbOfjWdypyz8mjPTrYCAAAoGK8HjaTUDI2Y8omt4C3mPIw3H75B85+9na1q/YBZRD3c86X917eHqmOrBrbru1o2rO75+3Uj4RUAAHiN14PGk9M+157EFFvBG85oXke/Tr1fPTq3tR34C3Po35fj+ztT3cwia19kds366KnbfPb/PgAA4J+8GjR27Dmg8TO+thUKKjTHpYdvuVALJw5UnaqBfWZDIDNPCcxUt+VThqht05q26xvM1K4Pn+zJUzIAAOB1Xg0awyfPV1qG21YoiIrRpfXFi/00ss8lzjaq8H/m6cbiVwY7552EyTc2Snj1wevUvkVdWwEAAHiP177Brtu2R2/NZztbb2jVqJqWTb1H557q+3P7kTcmNJrzTr5/9W7VrBhju8Xj3u4dmY4HAAAKjdeCxsOvzlN2CE8zCsocvrfolbuZyhLgzLbEK6YOdXYRKw5d2jfV0/262QoAAMD7vBI0ft24SzO/XWUr5IeZx//k7V2cw/ciw8NsF4GsfJkozXumr57o28VZj1NUzBkf7424RS4XO0wBAIDC45VvN6OmLuBwvgIoERaqqQ9117CbL7AdBAsTMB/scYGzHsesyyls5WIiNHdMb+c0cwAAgMJU4KCxeks8TzMKwJzyPW9sH+bKBzmzHuenKXerRb0qtuN9ZgH6zCd6q371CrYDAABQeAocNEZN/YynGflk7i5/8+JdurBtY9tBMDNbGC96ZbC6ntHMdrxrwpBr2GAAAAAUmQIFjU079+n9r9hpKj8qxETqqwmD1KpRDdsBzBOuCH30dG9nRyhvuuOy9up3xZm2AgAAKHwFChrPTf9GbrdXlnkElUqxUfrqhUE6pUE12wH+n1mkPfbOSzVl2PVeOW+jY6sGeuGeq2wFAABQNPKdEhIOpmjK/B9shdwyTzK+fmGAWtSvajvA0fXscrpmj+mrUpHhtpN3Zj3GjJG3cugjAAAocvn+9vHSh99zCngemYXfn4zrr2Z1Cm/BLwJLl/bN9NWE/s56nryKiQrXnKdvc7bRBQAAKGr5ChqZh7M18cNFtkJuRLhCNXt0X7VpUtN2gNwxh/t999LgPJ0k7nK59e6jtxBqAQBAsclX0PjfVyu0OzHJVjgRcxjbe6Nu0XmtG9oOkDcmMCyedLca16pkO8f31B2XOU9DAAAAiku+gsaLM76zI+TGc4Mv0xUdWtgKyJ8alcrqmxcHOCd7H8+tF7fVfTecZysAAIDikeegsXz9Di1Zu81WOJHe3U7XwGs62AoomCrlovXtxAHHPNjvjOZ1NGnodbYCAAAoPnkOGi/P+t6OcCIdTqmnifdcYyvAOyqWLX1k57J/hQ2zhuPDJ3spvESo7QAAABSfPAWN1PRMvf8lB/TlRu0qsXzpQ6ExO0mZsNGyYXWnjooI1dyxtztPPAAAAHxBnoLGe18sV1Jqlq1wLOElpA9G9WRbURQq8/fri+f7qVWjapr68M0cAAkAAHxKnoLG5Nkc0JcbT/e7nG1sUSRM2Phh0j26uuMptgMAAOAbch001m3bwyLwXDC7Sw2+9hxbAYWP6XkAAMAX5TpovL1gmR3hWKpVKKMpD3a3FQAAABC8chU0cnJy9M4CFoGfyNSHuqts6ZK2AgAAAIJXroLGD79t0R/x+22FozHnZVzYtrGtAAAAgOCWq6Dxv69+sSMcTa0KsRo38ApbAQAAADhh0HC7czTj619thaOZ9MC1ii4VYSsAAAAAJwwa36/crF37DtoK/3b1uSerc7smtgIAAABgnDBofPjtSjvCv5nTmJ8bxJQpAAAA4N9OGDRmf7fKjvBvw3tdrBqVytoKAAAAwJ+OGzRWbYrT1vhEW+HvGteqpLuvPddWAAAAAP7uuEFj9kKmTR3L6Du6ciIzAAAAcAzHDRrzFq22I/xd26Y1dUWHFrYCAAAA8G/HDBoJB1O0dP0WW+HvxvS/1I4AAAAAHM0xg8bnS9fL7T7uA4+g1On0Jjr31Aa2AgAAAHA0x0wSC5assSP83SO3XmRHAAAAAI7lmEHjs6Xr7Qh/6nBKPbVvUddWAAAAAI7lqEFj3bY9nAZ+FMNuvtCOAAAAABzPUYPGt79stCP8qVWjaurcromtAAAAABzPUYPGN8sJGv923w0X2BEAAACAEzl60PiZ9Rl/Vzk2Wleec7KtAAAAAJzIf4LGpp37FJeYYisYfS5txyngAAAAQB78J2j88BuH9P1daI5Lt1/W3lYAAAAAcuM/QeNHgsY/dO3QTDUqlbUVAAAAgNz4T9BYRND4h56XnGZHAAAAAHLrH0Ej83C2ftuww1YoFxOhi9s1sxUAAACA3PpH0Fi5aZeyFGIrXNuxNYvAAQAAgHz4R9BYumabHcG4uXMbOwIAAACQF/8IGsvXb7cj1K4Sq/Yt6toKAAAAQF78I2j8unGXHeHys1vYEQAAAIC8+ito5OTkaPVmgsafLj2ruR0BAAAAyKu/gsbmXQlKyci2VXArW7KkOrSsbysAAAAAefVX0Phtc5wd4eIzmygs9B+zygAAAADkwV/fpn//Y7cd4dKzWJ8BAAAAFMRfQWPtVoLGn85t1cCOAAAAAOTHX0Fj/dY9dhTcGteqpCrlom0FAAAAID/+Chrrtu61o+B2QeuGdgQAAAAgv5ygkXAwRQfS0pxGsDu3FUEDAAAAKCgnaPwRt98pIJ11Sj07AgAAAJBfTtDYQtBw1KwYw/oMAAAAwAvsE40Epwh2bZrWtiMAAAAABeEEja080XCcfhJBAwAAAPAGJ2js2HfQKYJd68Y17QgAAABAQThBYxdBw3Fqw+p2BAAAAKAgbNBIdIpgVjG6tMqXibIVAAAAgIJwud052r03yZbBq2m9SnYEAAAAoKBcCYdSlKUQWwavZnWq2BEAAACAgnLtPZBsh8GtcW2eaAAAAADe4tqTSNAwmtSqbEcAAAAACsq170CKHQa36hXL2BEAAACAgnLtTyJoGDUrl7UjAAAAAAXlSkxKs8PgFV0qTGVLl7QVAAAAgIJyHSBoqEbF8nYEAAAAwBtcSSnpdhi8qleIsSMAAAAA3uA6lJphh8GrYmxpOwIAAADgDa7kNIJGuehSdgQAAADAG1ypGYftMHjFxhA0AAAAAG9ypaVn2mHwiuWJBgAAAOBVroyMLDsMXrHRbG0LAAAAeJMrNZMnGiUjwu0IAAAAgDe4srJz7DB4RYSH2REAAAAAb3C5cwga4WGhdgQAAADAG1xud7YdBq/wEgQNAAAAwJtcWVk80SjBEw0AAADAq1xuETRcISF2BAAAAMAbXC7xJZt1KgAAAIB3ucLCCBqHs1inAgAAAHiTy+VifULmYYIGAAAA4E0u1id4ggZPNAAAAACvcoWFEjQyMrPsCAAAAIA3uCIjSthh8ErLyLQjAAAAAN7gKhlO0EhMSrMjAAAAAN7gKhkZbofBKzEp1Y4AAAAAeIOrFFOnlHiIoAEAAAB4k6tUJEFjP080AAAAAK9yRZeMtMPgtZ81GgAAAIBXucpGl7TD4LUjfr8dAQAAAPAGV3Qpnmhs33fAjgAAAAB4gyuWJxo6lJKppNQMWwEAAAAoKFfFsqXtMLht35NoRwAAAAAKylUuppQdBrdt8UyfAgAAALyFJxrWhu177AgAAABAQbkqlI2yw+C2estuOwIAAABQUK7KsdFyudy2DF5rt/JEAwAAAPAWl8sVooplytgyeK3ZyhMNAAAAwFtc5v+pWiHGKYLZ7sQkHUjmhHAAAADAG5ygUb0iQcP4ZcNOOwIAAABQEE7QqFkx1imC3dI12+wIAAAAQEE4QaNWFYKG8dPqrXYEAAAAoCCcoFG3anmnCHZL1263IwAAAAAFceSJRuWyThHstsYnau+BZFsBAAAAyC8naNSrXsEpIH2/8g87AgAAAJBfTtCoUi5apcMjnEaw+3LpOjsCAAAAkF9O0DAa1OaphvHN8g12BAAAACC//goaTWtXsqPg9vvWPazTAAAAAAror6DRqBZB409fLuOpBgAAAFAQfwWNJrUq2xHm/7DajgAAAADkx19B4+QGVe0I839cI7c7x1YAAAAA8ur/F4NXr6jwErYIcgkHU/T9ys22AgAAAJBXfwWN8BKhalyziq0w+7tVdgQAAAAgr/4KGkbLhjXsCLO/JWgAAAAA+fWvoFHdjvBH/H4tWb3VVgAAAADy4h9Bo02TmnYE461PltoRAAAAgLz4R9A4tVENhYSE2Arvf/mzsrLdtgIAAACQW/8IGtGlItSgejlbYd+hdC1YstZWAAAAAHLrH0HDOLNFPTuCMXX+EjsCAAAAkFv/CRpntKhjRzBmf/Or4hIO2QoAAABAbvw3aJxU145gZClEkz5abCsAAAAAufGfoHFS3SqKiQq3FYzJc35kUTgAAACQB/8JGi5XiM4+pb6tYOzad1CzF3KAHwAAAJBb/wkaxjktG9gR/vTMO1/ZEQAAAIATOWrQ6NiqoR3hT0vWbtNXP2+wFQAAAIDjOWrQaNWoBus0juLJaV/YEQAAAIDjOWrQMOs0zm/d2Fb405c/r9eytdttBQAAAOBYjho0jM7tmtgR/u6x1z+1IwAAAADHcsygcVFbgsbRfPzDan2/crOtAAAAABzNMYNGnarl1LBGeVvh7x54ea4dAQAAADiaYwYNo+uZze0If7do1RZ99B3nagAAAADHctygcelZBI1jGfbKPE4LBwAAAI7huEHjrJPrqWzJkrbC363ZulsTZiy0FQAAAIC/O27QCAt1qcvZTW2Ff3vsjU8Ul3DIVgAAAAD+dNygYVx1bks7wr8dSsnUkBdm2woAAADAn04YNC5u11SlwyNshX9774sV+mbFRlsBAAAAME4YNCLDw9StQzNb4Wh6P/W+UtMzbQUAAADghEHDuO78U+0IR7Np5z4NnTjHVgAAAAByFTQubteM3adO4KXZi5lCBQAAAFi5ChrhJUJ19QUn2wpHk5OTo15PvKek1AzbAQAAAIJXroKGcXOntnaEY/kjfr9uH/O+rQAAAIDgleugcfYp9VS3Sjlb4VjMLlSvzllsKwAAACA45TpohISEqGeX02yF4xn0zIf6deMuWwGFa/jk+c6GBAAAAL4k10HD6NnldIXm5Ol/JChluLN1zcNTdCA5zXaAwjF4/CyNevNzdRv6GuuDAACAT8lTaqhRqaw6n9nYVjieDTsSdP2j0+R259gO4F0mZIyfsdAZr9m6W91HvMnfNwAA4DPy/HiiT7cz7AgnsmDJWt3zwmxbAd7z95Dxp3mL1+j+l+faCgAAoHjlOWh0O7O5aleJtRVOxHwZZHE4vMVso9xv7Iz/hIw/PfPe15r26VJbAQAAFJ88Bw2XK0T9rjjTVsiNO8f8T/MWr7YVkD9mWlSvJ9/TKx8dP7j2ffJ9LV71h60AwP9lZ7vtCIA/ydfK7t5d2ynCFWornEiWQnTtI2/y5Q/5lnk4W9c9MlVTPznx0wqzGcGVD76hHXsO2A4AAEDRy1fQKF8mSjddzAF+eZGanqku90zWqk1xtgPkjtlN6pJ7X9UH36y0nRPbnZikbve/5vy9AwB/l8J7GeCX8hU0jHu6n+OcrYHcO5CWps5DJnHmAXItfn+Szh0wQV/+vN52cu+XDTt1y6h3nHUdAODP0tIP2xEAf5LvoNGsThVdckYTWyG3du07qHMGjCds4ITWbdujdn2f0/L1+T/80TwFeeyNBbYCAP90MIVzqQB/lO+gYdzb/Tw7Ql7s3Juss+943vkiCRzNNys2qt1tz2trfKLt5N/jb3ymD77+1VYA4H/2JxE0AH9UoKBx7qkNdHqTWrZCXsQlpuicO1/U6i3xtgMc8frHP+qiu19xptp5g5k6dcsT72r5+h22AwD+Zff+JDsC4E8KFDSM4b062RHyyizYPbPPBH2/crPtIJhlZbs14NkP1Pup93U4K9t2vcMsCu829DVnzQcA+Ju4fYfsCIA/KXDQ6NK+mVo1qmYr5JW5a33BgJc0a+Eq20EwSjiYoovuflkTZy2yHe8z64Mu94QNs1UuAPiTPQeSlZ6ZZSsA/qLAQcN4+NbOdoT8MOceXP3wa3rhGKc9I7AtWb1Vp946Rl8v32g7hWfJ2m3q/dR0WwGAfzBTQLfG77cVAH/hlaBx+dnNeapRQG63S4PGz1K/sTO44xxEJn74vTrc+by27y26aQFvLVimse9+ZSsA/sLlnY9sv7V26247AuAvvPKuZc7TeKJvN1uhIF75aLHOG/Qic+kD3IHkNF3z8BsaMG6mJ1jaZhF64JWPNG/xalsB8AdhoWF2FJx+/4OgAfgbr90e6dyuic5sUcdWKIhFq7aoda9nnSk1CDwLf9mklreOzdNJ395mnqB1H/EGu54BfiQsLLgPyV22dpsdAfAXXn0OO6b/pXaEgjILd8/u/4KefudLTnYOEGZK3LBXPlbHQRO8cj5GQSWlZqnrvZOdhegAfF94aKgdBaef1vxhRwD8hVeDRvsWdXVFhxa2QkGZLU4fePljXTj4FcUlsLWfP/t14y61ue0ZPfX2l87TBF/xR/x+Xf3QVNYFAX6gRHgJOwpO5rDbTTv32QqAP/D6N56xd16qEmHBfdfF2778eb1Ovnk0W+D6IfMFfsSUT9W29zit2uyb05S++WWj7nr+Q1sB8FUxpSLsKHiZz0MA/sPrQaN+9Qrqf8WZtoK37DuUrisfnOIsIGahuH8wBzGatRiPTVng9QP4vM1sQmB2wALgu2JKRdpR8Jr/wxo7AuAPCmUOx4jbOqtidGlbwZvMAuKTbnpCb8xbYjvwNWbNQ88n3lWHO1/UGj/ajnHwuA/01c8bbAXA10RH8UTj00VrlJSaYSvkltud43xvYGtzFLVCCRplS5fU6Du72Aretv9QhnqNnq7zBk3Uqk1xtovilpXtdg5dbNR9pKZ+stTvFvFnKURXPfAGc6ABH1UmqqQdBS9zwO0HX/9iK+TGNys2qvVtzzrfG5rVqWK7QNEotFWpvbqcrrZNa9oKhcGcJH1qj2ecQ/7YOah4mTMpTu4xxjl00QRBf3UgLU1d7pvsnPMBwLdUKBtlR8Ft0qzFdoTj2RK335ly3XHgRP2yYadCc1zq0LK+vQoUjUILGuYQv5eGXCOXy207KAzZIW5nfn29ax93HommpmfaKygKy9ZuV8cBE9V16GS/miZ1POu27dENj05zHrUD8B3VKpSxo+C2ZO02zpk6DvM9YPjk+Wpy3ZP/2ESmVbPqimZDARSxQgsaRpsmNTXwqnNthcJ0KCVTQ1+aqwbXPO5M32G70sJlpqxdPux1Zzcps2tToPlkyVrdO/EjWwHwBVXKx9gRRr3xmR3hT3+uwzDTd0e9+bkzzezvzm/dyI6AolOoQcMY2ecS1azIm2NRiUtMcabv1Lt2hF6etUjpmVn2Crxh+fodzqPoU24dq4+++812A9Nz73/LpgOAD6kcyyYrf/r4h9XOzn444vOl6/5ah2HOGzmac1o2sCOg6BR60DCP6V6+7zpboaiYN5r+z36gWpc95jxCZUvcgjFv4p2HTFLrXs86j6KD5bT2O56ZrsWrOI0X8AW1KsfaEYz+z8xwNuEIZmYKmdkY5qK7X3HWYRyLOd+sQ8t6tgKKTqEHDaNL+2bqfsGptkJR2puU7DxCrXP5Y+o9+n3nhGrkjpnn+uqcxTq5x9POm/iCJWvtleCReVi64sHXtW13ou0AKC5mjUaQHw7+D+YQ1NHTPrdVcDHrA7sOfVXt+j7vbAxzIqc1ralSkeG2AopOkQQN44W7r1LVWHbMKC5mrubr8350DpBr3WusczgbOwsdnVl/MXj8LFW9bLhuHzPDZ0/0Lip7ElPU9b5X2WgAKGYuV4jqVq1kKxiPv/6JFv6yyVaBzzzBMAHDrA+ctzj3hxd2bNXQjoCiVWRBo3yZKE0edoOtUJyWr9+lAeNmqsolw3XDiGn66LtVQb94fO+BZGcR/Wl9xunkW8ZovGdsFtjjCBO2bnr87aCZMgb4qgY1ytsRDHP+zzXDA//8n09/XOtMkTJPMPISMP50fhsWgqN4FFnQMMwUqtu6tLMVipt5yvHeFyt0+bApqtDlASd0mPUHwXLn2pw9YqZGmbUX1bo+7CyiX7pmu72KfzN/Nx557RNbASgODapXtCP8yTx17XTPJO3Yc8B2AoP5LH794x/V/MbRuvjeSbmaInU0Ea5QtW9e11ZA0SrSoGE8d9flasgdGZ+TlJrlhA6zo1K5ix7UhXe95JzLEWgnj5v/PE+/86XO6jdelbo96EyNMmsvzF0xnJhZ7zPd8/cEQPFoVreyHeHvzBONs/pPcM4B8nfmP8t9E+eo5pWPqPdT7+v3rQX7z9S+ZV2Flwi1FVC0QnKKYS6EmWN4Vt/n+HLnJyrHRuu81g10WrPaOqN5HZ3asIbfvGmZN+xvVmzUtys26Yuf1jjb/6JgSka4tHDiYOecHH/y/P++1d0TZtsqOC197R6/+3PDP5nPTzN9BkdXtmRJTX2kuy47u4Xt+Afz9OLDb1dqyrwlns+sTV6dpvr4bZ01vGcnWwUGc7Nw0aottgpOcXMeV5Vy0bbyXcUSNIwnp32hh16dZyv4E/MYts1JNdWyQXWdVK+qWtSvquaen2VLl7S/UTzMzkgr1u/Qyo27tGTNVi1ds815pA7vM7vfLHv9HlX1owPECBoEjUCQlJqhMp2GsV7qBHp3O11j77ys2D+XjsccsPflz+v17mc/a+a3K5yZBYVh0cuD1L5FYE2dImgQNE7I/H978b2vBuWWoYHKfPlsUL28alcp53nFqk7Vcs6+79UrllFsdCmVj4kq0JMQs0vWrn0HFZ+QpK3x+7XVEyw27tirNVvitWHHnkJ7k8bRtW1aUwsnDlJkeJjt+DaCBkEjUDS47omAX/zsDRWjS+vRPp3Up9sZPvMU3jy5MGstZn27Sh9994v2HUq3VwpHVESo9n/6dMBNnSJoEDRyxSzGPeXWp455iiUCj9nH29xhio2KUInwEooIDVVERJjCw478zMrOUVaWW4ezspSclq6U9CwlJqVof2IyU+18kDkf590RPWzl2wgaBI1AYTbuMGvqkDtma/1+V56t3peeUeRPYc1XrJWb4vTlsvX6bOk6fbN0g7MRS1HpdHoTffrs7bYKHAQNgkaufb9yszr2n8CXSMBPPXl7Fw27+QJb+S6CBkEjUJituM0uecgbl8utc1o20qVnNddFpzVW09qVFRLi3e8eZqv0n9fu0NI1W50pvItWbNGBtOI7s2pM/26674bzbBU4CBoEjTzhTRPwX+aDetaTPX1+8SVBg6ARKMyp0ObANhRMuZgItWpYSy0b1VC9auVVt2p5Va0Qo0qxpVW6ZISiIsOdQxINs54iJT1TyWkZSkxK1Z7EZGc73W27D+iPuH1av22v1mzeo71JvjVDI1D/zRM0CBp51vOJdzX1k6W2AuBPSodHaPHku5yNAXwVQYOgESjMAauxFz/Iaf1F4M8nHj7yVSlPzA5cCQue+CssBRKChv8EjSI/R+NYXr7vWmdxKQD/k5yZoW73T3bWXQEoXGZh79mn1LMVCpMJGP4YMoxz2tQPyJAB/+IzQcPsXPPhE72cnYsA+J+t8YnOgY/mbiuAwnV+64Z2BBzduac2sCOg+PhM0DBqVCqruWN6OzsTAfA/C3/drDvHzbAVgMJy0WlN7Ag4uvNbN7IjoPj4VNAwWjWqobcfucnrO0EAKBqvzV3inF4MoPCcXL+qKsf6/vxsFI8KMZFqXq+KrYDi43NBw7iiQwuNG3iZrQD4E3Mi7+nNatsKQGEwN+O6ndXMVsA/nduqETds4RN8MmgYg689R/d272grAP7APKqfeM81tgJQmC4/+2Q7Av7pPKZNwUf4bNAwzEEz5uRhAL6vRb0q+uCJW50dcQAUvgvbNlZ0qTBbAf/vPDYLgI/w6aBhHvtNfehGdWnf1HYA+KKqsVGa/8ztKlu6pO0AKGwm1Hdt79sHZaLomffjxrUq2QooXj4dNAzzRvrBqF46tyXbtAG+KCYqXJ8819/ZNQ5A0erR+TQ7Ao44l2lT8CE+HzQMc8bGnDG9OdAP8DERrlDNfbqvTmlQzXYAFKWLTmvM7lP4h/PbEDTgO/wiaBjRpSL02XP91KoRX2gAX+ByufXeqFvUoWV92wFQ1MzJz9ezlhF/w0F98CV+EzQMM//7ywkDCBtAMTPrp14d2t3ZihpA8ep72Rl2hGBXu0qs6levYCug+PlV0DAIG0DxM+fc3Na1na0AFKdmdaqowyn1bIVg1pGnGfAxfhc0jD/DxhnN69gOgKLy+G2dnXNuAPiOgVd3sCMEs45sawsf45dBwzBh44vn+zkHhAEoGg/cdL6G9+xkKwC+4vIOLVStQhlbIVid14qgAd/it0HDKBUZrvnP9NVlZze3HQCFxYSM0Xd0tRUAXxIW6tKQ68+1FYJRwxrl2WYcPsevg4Zhztn48Ile6t3tdNsB4G33du9IyAB83B2Xt1f5MlG2QrA5h/UZ8EF+HzQMs73f5Puv18O3XGg7ALzFPMkYe+eltgLgq8xT/gFXnWUrBJvzWze2I8B3BETQ+NPIPpdo0tBrFKYc2wFQEGbhN08yAP9hNmooFxNhKwST89uwPgO+J6CChtH30vaa/1w/xUSF2w6A/Hh2wGUs/Ab8jNko5YGbLrIVgsVJtSupYtnStgJ8R8AFDePCto31w6S7VbdKOdsBkFuhOS699sB1uoeFpYBfuvPKs1S9Il86g8m57DYFHxWQQcMwBxgtff0edWzF4iggtyJcoZr51K0cxgf4MbNWY2RvpjwGk/Pbsj4Dvilgg4Zhdt/47Ll+uusaDjICTsRMN/xsQj9ddnYL2wHgr2695DSd3qSWrRDIXC43J8PDZwV00DDM3uLP33WF3hp+o3OXB8B/1a4S60w37NCyvu0A8GchISF6aeg1zpdQBLaT69dkW2P4rIAPGn+6qVMb/ThpsHOgDYD/17ZpTf346t3OdEMAgaNVoxrqd9nZtkKgYoo4fFnQBA2jRf2q+nnKfep+wam2AwQ3c6r+Ny8MUJVy0bYDIJCM7tfNeWKJwHV+60Z2BPieoAoaRnSpCL07ooemDLueqVQIauaAy1lP9uLfARDAzGfelGHdnalUCDzm3DCmvMKXBV3Q+FPPLqdr+ZQhatmwuu0AwaFkhEvvecK2OeCSLx9A4DuvdUP1v7y9rRBIWjep7YRJwFcFbdAwGteqpCWv3q0HbjqfBXMICuZsme9fvlvXM30QCCrPDLxcLeqxDivQdOQ0cPi4oA4aRniJUI2+o6u+mzhY9atXsF0g8HRp31TLp97rLBAFEFwiw8M0Y1RPRUWE2g4CgXlaBfiyoA8af2rfoq5Wvnmfc+YGTzcQSMxJ34/f1llzn+6jsqVL2i6AYGOe4r827EZbwd+Fl5DO9Hx3AXwZQeNvzKJYc+aGebph3pABf1etQhl9NbG/hvfsxHoMAM60STNdGP6vXbN6bOYBn0fQOIojTzeGOneBI1w8ZoZ/MlvXmqd07EgC4O+e6NtFXc9oZiv4q45Mm4IfIGgcg1m7Ye4C//7eA7qAf8zwI2YO9qSh12j26Ns4LRbAf7hcIXr3sR5q1aia7cAfndeK7ybwfQSNEzALxD8f318zRt7KoUfweWc0r6Nfpz2gvpeylSWAYzNbon46rp8a1ihvO/AnZpvydifVsRXguwgauXR1x1O0+u0H9GivTs4/cMCXmCl+T/Xrqu9fGsTuaQBypWLZ0vrsuf6qGsuTT3/TvnkDZ+YF4Ov4xpwHZtHViF6dtf694bq5Uxt2p4JPOLNFHf369lDdf6M5D4YF3wByr07Vcvp64iBn4wj4D7a1hb8gaORDjUplNW34jVr62n2s30CxiYkK10tDrtZ3Lw1ilzQA+WbePxZOHEDY8CPnnsomH/APBI0CMAefmfUbX79wp05vUst2gcJ340WttO69h9XvijPZthZAgZkplyZssGbD95UOj9BpzWrbCvBtBA0vOPfUBvrxtbv18Zg+7OKBQtWiXhUn2L79yM2qUi7adgGg4EzYWPTKYLVtWtN24IvOPrWuwkL5+gb/wN9UL+rSvpl+nnKfEzh4o4Y3VYwu7WxZ+8vUoU6wBYDCYBaIf/PCAOccHvgmzs+APyFoFAITOH6afI8+e+4OdWzFl0Lkn9lN6t7uHbV+xoPOlrUs9gZQ2MzGJ7Oe7OXsssjUTN9zfptGdgT4PoJGIbqwbWN9NeFOLX3tHl197skKzeG/buSO+btyW5d22vjBwxp756UqW7qkvQIAhc8EDLPL4tynezsbT6D4Na1dWV+O7++sDwX8Bd98i0CbJjU1Y1RPbf7wYefudNmSfGnE0Zktk7tfcKpWvnOfXht2nbPDGQAUF/OE/tep9zvbaKN4mKmzZofBldOGsq0t/A5BowjVqhzr3J3eOXeE86ZhFvYCxp8BY9W0YXp3RA81q8PfDQC+wZy1sXDiID1+W2eFKcd2UdiiIkL18C0XatPMIzsMsgAc/oi/tcXAzH81bxorp92vRS8Pcg7/M3PxEXzMn/sdl7V3DoEkYADwVWZ92PCenbRs6lA2Oylk5nPhrms66I+ZIzSyzyWKLhVhrwD+h6BRzNq3qOsc/hc/f6TzlIM38OBQKTbKWWi5fc4IvXzfNc62kgDg605pUE0/Trpbzw263DnPAd5jbkI6AWPWI3r+riucHcAAf0fQ8BFmsa95ymF2q/r97fv1wE3nq3aVWHsVgcIEybc8wXL7h485Cy35IAHgb8zTjcHXnqMNHzyk3t1OZ6OTAjJrMMyNp20zjwSMquVj7BXA/4XkeNgxfIz5o/nhty1697Of9eE3vyguMcVegT8xO7bccEEb9bnsDHYLKUbP/+9b3T1htq2Ck9kBz2xOAXjTqk1xGjZpruYtXmM7yI2WDatrwFVn6caL2igyPMx2kRtn9RuvRau22Co4xc153C8O7iVo+Am3O0ffr9ysD79dqVlfr9K2fYn2CnyRWdzd8dQm6nFxG1197inOI3EUL4IGQQOFa/n6HXr8jQX66LvfbAf/Zj4Lru14ivpceoYzdRr5Q9AgaKCQ/bpxl+Z8t0pzF/+uZWt3OE8/UPxaNaqm6y9orZs6teHxt48haBA0UDTME44JH3yrdz5bqrQMt+0GL3PjqcPJjY7ceOrYksXdXkDQIGigCO09kKwvl23Q/B9W67Of1ml3YpK9gsJmDrVqd1Jt56nFleec7GwDCd9E0CBooGglHEzRlHlL9PrHS7Ru2x7bDQ5m3cqZLes4nw1Xn9eSG09eRtAgaKAYrd4Sr6+Xb/SEj/Va+Otm580e3mPWXFzUtokuOaOZLva8/OEfOo6secp2B/fbHfvwo7gsW7tdby9Ypve++Fl7AnS9YYWYSF14WlN1btdUXTyfDeXLRNkr8DYzndwd5F9f/eX9nKAR4Mwf75qtu51F5d97QseiVZu1ced+plrlQXgJqV2zerqgTSOd73m1aVLL0+PcEwDIK/MF8cfft2jWwlWau+h3v37SYYLF2S0b6qyT6zmfDSfXr+o85Qbw/wgaQehAcppzd2npmm3O65f1O7VldyLhwypbsqTOOLm2zjqlvucDpK4z1YTF3ADgfds8nz3frNior3/eoG9XbNIf8fvtFd9iPhdaNq6uUz0vs3vgGc3rcP4RkAsEDTiSUjP068adziLzNX/Ea/XW3Vr7R1zAb6lbNTZKJ9WrpjZNa6l145o6tVF1PjwAoJiYG2HL1+3Qz+u2O59J67fv1YYt+3QgLc3+RuGJLhWmGhXLq171cqpTuZwa16qkJnUqq2ntyqpRqaz9LQB5QdDAcZk3/c27ErR5Z4Lzhm9+bonfr227E7Rt1wFluLPtb/qukhEu1alSUY1qVVTDGp5XzQpqUquyTqpbhTm0AOAHzFrDrbsTFbfvkOL3H9KuvQeV6Pl8SkpNV0pappJSMnTYfWSHq+ysbJUIC5UrNERhoaEqFVFCESXCFBUZruioSE+giFRsdEnnwNRKsaVVuVy0qlUow25QQCEgaKBAzI5X8QlJ2ul5049LOKg9nnr3/iQlHkrVvgMpSkhK1YGkNCWnpumg5wMhOS1dbnfBFzCVDo9QTEykyseUVIzng6NSbLQqm1f5aOfDw3xomJPVa1Yqy+nbAAAARU76P0gs5NPPf2iiAAAAAElFTkSuQmCC";
                                //Entity.response.ACTIVITY_DATE = DateTime.Now.ToString();
                                //ViewBag.ModalProperty = "LoadModal";
                                //return View(Entity);
                                #endregion

                                #region Production
                                message = VerifyFingerPrints(Entity);
                                #endregion
                            }
                            else
                                message = "Request limit exceeded for Citizen No # " + Entity.CitizenNumber + ".";
                            return View(Entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Verification", "Biometric", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
    
        #region SENDING DATA TO NADRA 

        public string VerifyFingerPrints(VerificationData Entity)
        {
            string newCode;
            string xmlSource;
            string Requesttime, Responsetime;
            string message = "";
            try
            {

                BioVeriSysBranchBankAccountClient.BioVeriSysBranchBankAccountClient bvs = new BioVeriSysBranchBankAccountClient.BioVeriSysBranchBankAccountClient();

                Random random = new Random();
                Session["Thumb"] = HttpContext.Session["Value"];
                if (Session["Session_NADRA"] != null)
                {
                    newCode = Session["Session_NADRA"].ToString();
                }

                else
                {
                    //newCode = "2254" + random.Next(1000000, 9999999).ToString("000000000") + random.Next(10000000, 99999999).ToString("0000000000");
                    newCode = "";

                }


                string Trnxsid = "";
                if (Session["Trnxsid"] != null)
                {
                    Trnxsid = Session["Trnxsid"].ToString();

                }
                else
                {

                    Trnxsid = "2371" + random.Next(1000000, 9999999).ToString("0000000") + random.Next(10000000, 99999999).ToString("00000000");
                    Session["Trnxsid"] = Trnxsid;
                }



                //	$user_name = 'citi_branch';
                //$pass_word = 'N5j52WYFb!a';
                //franchizeID' => '2371

                string FRANCHISEEID = "";
                string username = "";
                string pwd = "";
                string templatetype = "";
                BIO_NADRA_CONFIGRATION Detail = context.BIO_NADRA_CONFIGRATION.Where(m => m.NC_ID == 1).FirstOrDefault();
                if (Detail != null)
                {
                    FRANCHISEEID = DAL.Decrypt(Detail.NC_FRANCHISE_ID);
                    username = DAL.Decrypt(Detail.NC_USER_NAME);
                    pwd = DAL.Decrypt(Detail.NC_PASSWORD);
                    templatetype = DAL.Decrypt(Detail.NC_TEMPLATE_TYPE);
                }
                if (Entity.CitizenNumber == null)
                {
                    message = "Citizen Number is required!";
                    return message;
                }
                if (Entity.ContactNumber == null)
                {
                    message = "Contact Number is required!";
                    return message;
                }
                string cnic = Entity.CitizenNumber.ToString().Replace("-", "");
                string contact = Entity.ContactNumber;
                Session["contact"] = Entity.ContactNumber;
                Session["CNICNO"] = Entity.CitizenNumber.ToString().Replace("-", "");
                string fingerindex = "";
                if (Entity.RightThumb == true)
                    fingerindex = "1";
                else if (Entity.RightIndex == true)
                    fingerindex = "2";
                else if (Entity.RightMiddle == true)
                    fingerindex = "3";
                else if (Entity.RightRing == true)
                    fingerindex = "4";
                else if (Entity.RightLittle == true)
                    fingerindex = "5";
                else if (Entity.LeftThumb == true)
                    fingerindex = "6";
                else if (Entity.LeftIndex == true)
                    fingerindex = "7";
                else if (Entity.LeftMiddle == true)
                    fingerindex = "8";
                else if (Entity.LeftRing == true)
                    fingerindex = "9";
                else if (Entity.LeftLittle == true)
                    fingerindex = "10";

                string area = "";
                if (Entity.AzadKashmir == true)
                    area = "azad kashmir";
                else if (Entity.Balochistan == true)
                    area = "balochistan";
                else if (Entity.Fata == true)
                    area = "fata";
                else if (Entity.GilgitBaltistan == true)
                    area = "gilgit-baltistan";
                else if (Entity.KhyberPakhtunkhwa == true)
                    area = "khyber-pakhtunkhwa";
                else if (Entity.Punjab == true)
                    area = "punjab";
                else if (Entity.Sindh == true)
                    area = "sindh";
                //CLOSE KYA HAI 
                //string accountype = "";
                //for (int i = 0; i < ddlAccounttype.Items.Count; i++)
                //{
                //    if (ddlAccounttype.Items[i].Selected)
                //    {
                //        accountype = ddlAccounttype.Items[i].Text;
                //    }

                //}

                //lblerr.Visible = true;
                //lblerr.ForeColor = System.Drawing.Color.Red;
                //message = HttpContext.Current.Session["Value"].ToString();

                xmlSource = @"<BIOMETRIC_VERIFICATION><USER_VERIFICATION><USERNAME>" + username + "</USERNAME><PASSWORD>" + pwd + "</PASSWORD><FRANCHISEE_ID>" + FRANCHISEEID + "</FRANCHISEE_ID></USER_VERIFICATION> " +
                                   " <REQUEST_DATA> " +
                                       " <TRANSACTION_ID>" + Trnxsid + "</TRANSACTION_ID> " +
                                       " <SESSION_ID>" + newCode + "</SESSION_ID> " +
                                       " <CITIZEN_NUMBER>" + cnic + "</CITIZEN_NUMBER> " +
                                       " <CONTACT_NUMBER>" + contact + "</CONTACT_NUMBER> " +
                                       " <FINGER_INDEX>" + fingerindex + "</FINGER_INDEX> " +
                                       " <FINGER_TEMPLATE>" + Session["Thumb"] + "</FINGER_TEMPLATE> " +
                                       " <TEMPLATE_TYPE>" + templatetype + "</TEMPLATE_TYPE> " +
                                       " <AREA_NAME>sindh</AREA_NAME> " +
                                       " <ACCOUNT_TYPE>Current</ACCOUNT_TYPE> " +
                                       " </REQUEST_DATA> " +
                                   " </BIOMETRIC_VERIFICATION>";


                Requesttime = DateTime.Now.ToString("h:mm:ss tt");
                DateTime Reqttime = DateTime.ParseExact(Requesttime, "h:mm:ss tt", null);

                string response = bvs.VerifyFingerPrints(FRANCHISEEID, xmlSource);


                Responsetime = DateTime.Now.ToString("h:mm:ss tt");
                DateTime ResDate = DateTime.ParseExact(Responsetime, "h:mm:ss tt", null);

                Session["XMLResponse"] = response.ToString();

                var diffInSeconds = (Reqttime - ResDate).TotalSeconds;
                Session["TimeDifference"] = diffInSeconds;
                message = decode(Entity);
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Verification", "VerifyFingerPrints", Session["USER_ID"].ToString(), ex);
            }
            return message;
        }

        #endregion

        #region DECODE NADRA DATA 

        public string decode(VerificationData Entity)
        {
            string message = "";
            try
            {
                Entity.response = new VerifiedClientFromNadra();
                string strXMLDataTable = Session["XMLResponse"].ToString();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(strXMLDataTable);

                string xpath = "BIOMETRIC_VERIFICATION/RESPONSE_DATA/RESPONSE_STATUS";
                var nodes = xmlDoc.SelectNodes(xpath);

                foreach (XmlNode childrenNode in nodes)
                {
                    if (childrenNode["CODE"].InnerText == "102")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "103")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "104")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "201")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "202")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "105")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "106")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "107")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "108")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "109")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "150")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }


                    if (childrenNode["CODE"].InnerText == "155")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }


                    if (childrenNode["CODE"].InnerText == "171")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }


                    if (childrenNode["CODE"].InnerText == "173")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }


                    if (childrenNode["CODE"].InnerText == "174")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "110")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "111")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "112")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "114")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "115")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "118")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "119")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "120")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "123")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "124")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "125")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }


                    if (childrenNode["CODE"].InnerText == "175")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "185")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "186")
                    {
                        message = "Unsuccessful Response : " + childrenNode["MESSAGE"].InnerText;
                    }

                    if (childrenNode["CODE"].InnerText == "121")
                    {
                        string strXMLDataTablecit = Session["XMLResponse"].ToString();
                        XmlDocument xmlDoccit = new XmlDocument();
                        xmlDoccit.LoadXml(strXMLDataTablecit);

                        string xpathcit = "BIOMETRIC_VERIFICATION/RESPONSE_DATA/FINGER_INDEX/FINGER";
                        var nodescit = xmlDoccit.SelectNodes(xpathcit);

                        XmlNodeList nodescitses = xmlDoccit.GetElementsByTagName("SESSION_ID");
                        for (int i = 0; i < nodescitses.Count; i++)
                        {
                            Session["Session_NADRA"] = nodescitses[i].InnerXml;

                        }

                        XmlNodeList nodescits = xmlDoccit.GetElementsByTagName("FINGER");

                        for (int i = 0; i < nodescits.Count; i++)
                        {

                            message += nodescits[i].InnerXml.Replace("10", "0") + "|";

                        }

                        message = "Registered Fingers are: " + message.Replace("1", " Right Thumb ")
                       .Replace("2", "  Right Index  ")
                       .Replace("3", "  Right Middle  ")
                       .Replace("4", "  Right Ring  ")
                       .Replace("5", "  Right Little  ")
                       .Replace("6", "  Left Thumb  ")
                       .Replace("7", "  Left Index  ")
                       .Replace("8", "  Left Middle  ")
                       .Replace("9", "  Left Ring  ")
                       .Replace("0", "  Left Little  ");

                    }



                    if (childrenNode["CODE"].InnerText == "122")
                    {
                        string strXMLDataTablecit = Session["XMLResponse"].ToString();
                        XmlDocument xmlDoccit = new XmlDocument();
                        xmlDoccit.LoadXml(strXMLDataTablecit);
                        string xpathcit = "BIOMETRIC_VERIFICATION/RESPONSE_DATA/FINGER_INDEX/FINGER";
                        var nodescit = xmlDoccit.SelectNodes(xpathcit);
                        XmlNodeList nodescitses = xmlDoccit.GetElementsByTagName("SESSION_ID");

                        for (int i = 0; i < nodescitses.Count; i++)
                        {
                            Session["Session_NADRA"] = nodescitses[i].InnerXml;
                        }

                        XmlNodeList nodescits = xmlDoccit.GetElementsByTagName("FINGER");
                        for (int i = 0; i < nodescits.Count; i++)
                        {
                            message += nodescits[i].InnerXml.Replace("10", "0") + "|";

                        }

                        message = "Registered Fingers are: " + message.Replace("1", " Right Thumb ")
                                          .Replace("2", "  Right Index  ")
                                          .Replace("3", "  Right Middle  ")
                                          .Replace("4", "  Right Ring  ")
                                          .Replace("5", "  Right Little  ")
                                          .Replace("6", "  Left Thumb  ")
                                          .Replace("7", "  Left Index  ")
                                          .Replace("8", "  Left Middle  ")
                                          .Replace("9", "  Left Ring  ")
                                          .Replace("0", "  Left Little  ");
                    }

                    if (childrenNode["CODE"].InnerText == "100")
                    {
                        string strXMLDataTablecit = Session["XMLResponse"].ToString();
                        XmlDocument xmlDoccit = new XmlDocument();
                        xmlDoccit.LoadXml(strXMLDataTablecit);

                        string xpathcit = "BIOMETRIC_VERIFICATION/RESPONSE_DATA/CITIZEN_NUMBER";
                        var nodescit = xmlDoccit.SelectNodes(xpathcit);

                        XmlNodeList nodescits = xmlDoccit.GetElementsByTagName("CITIZEN_NUMBER");
                        for (int i = 0; i < nodescits.Count; i++)
                        {
                            //txtcn.Text = nodescits[i].InnerXml;
                            string a = nodescits[i].InnerXml;

                        }

                        string strXMLDataTableper = Session["XMLResponse"].ToString();
                        XmlDocument xmlDocper = new XmlDocument();
                        xmlDocper.LoadXml(strXMLDataTableper);

                        string xpathper = "BIOMETRIC_VERIFICATION/RESPONSE_DATA/PERSON_DATA";
                        var nodesper = xmlDocper.SelectNodes(xpathper);

                        foreach (XmlNode childrenNodeper in nodesper)
                        {
                            LogVerification(Entity.CitizenNumber);
                            Entity.response.NAME = childrenNodeper["NAME"].InnerText;
                            Entity.response.FATHER_OR_HUSBAND_NAME = childrenNodeper["FATHER_HUSBAND_NAME"].InnerText;
                            Entity.response.PRESENT_ADDRESS = childrenNodeper["PRESENT_ADDRESS"].InnerText;
                            Entity.response.CNIC_NO = Session["CNICNO"].ToString();
                            Entity.response.PERMANANT_ADDRESS = childrenNodeper["PERMANANT_ADDRESS"].InnerText;
                            Entity.response.DATE_OF_BIRTH = childrenNodeper["DATE_OF_BIRTH"].InnerText;
                            Entity.response.BIRTH_PLACE = childrenNodeper["BIRTH_PLACE"].InnerText;
                            Entity.response.EXPIRY_DATE = childrenNodeper["EXPIRY_DATE"].InnerText;
                            Entity.response.IMAGE_URL = "data:image/png;base64," + childrenNodeper["PHOTOGRAPH"].InnerText;
                            //TextBox9.Text = childrenNodeper["EXPIRY_DATE"].InnerText; ;
                            Session["Photo"] = "data:image/png;base64," + childrenNodeper["PHOTOGRAPH"].InnerText;
                            Entity.response.ACTIVITY_DATE = DateTime.Now.ToString();
                            ViewBag.ModalProperty = "LoadModal";
                        }
                        Session.Remove("GetTemp");
                        Session.Remove("Thumb");
                        Session.Remove("XMLResponse");
                        //Session.Remove("Photo");
                        Session.Remove("TrnxID");
                        Session.Remove("Trnxsid");
                        message = "Verification Success.";
                    }


                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Verification", "decode", Session["USER_ID"].ToString(), ex);
            }
            return message;
        }

        #endregion

        public void LogVerification(string CNICNo)
        {
            int RowCount = 0;
            BIO_REQUEST_LOG CheckLog = new BIO_REQUEST_LOG();
            CheckLog = context.BIO_REQUEST_LOG.Where(m => m.RL_CLIENT_NO == CNICNo && System.Data.Entity.DbFunctions.TruncateTime(m.RL_REQUEST_DATETIME) == System.Data.Entity.DbFunctions.TruncateTime(DateTime.Now)).OrderByDescending(m => m.RL_ID).FirstOrDefault();
            if (CheckLog == null)
            {
                BIO_REQUEST_LOG Log = new BIO_REQUEST_LOG();
                Log.RL_ID = Convert.ToInt32(context.BIO_REQUEST_LOG.Max(m => (decimal?)m.RL_ID)) + 1;
                Log.RL_CLIENT_NO = CNICNo;
                Log.RL_AMENDMENT_NO = 0;
                Log.RL_REQUEST_DATETIME = DateTime.Now;
                context.BIO_REQUEST_LOG.Add(Log);
                RowCount += context.SaveChanges();
            }
            else
            {
                if (CheckLog.RL_AMENDMENT_NO < 15)
                {
                    BIO_REQUEST_LOG Log = new BIO_REQUEST_LOG();
                    Log.RL_ID = Convert.ToInt32(context.BIO_REQUEST_LOG.Max(m => (decimal?)m.RL_ID)) + 1;
                    Log.RL_CLIENT_NO = CNICNo;
                    Log.RL_AMENDMENT_NO = Convert.ToInt32(context.BIO_REQUEST_LOG.Where(m => m.RL_ID == CheckLog.RL_ID).Max(m => (decimal?)m.RL_AMENDMENT_NO)) + 1;
                    Log.RL_REQUEST_DATETIME = DateTime.Now;
                    context.BIO_REQUEST_LOG.Add(Log);
                    RowCount += context.SaveChanges();
                }
            }
        }

        public void SetSession(string value)
        {
             HttpContext.Session["Value"] = value;
            string ActivityTime;
            ActivityTime = DateTime.Now.ToString("h:mm:ss tt");
            DateTime ActTime = DateTime.ParseExact(ActivityTime, "h:mm:ss tt", null);
            HttpContext.Session["ActivityTime"] = ActTime;
            // HttpContext.Current.Response.Write(value);
        }


        #endregion

        #region Nadra Configration
        public ActionResult Configration(int C = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Verification", "Configration", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (C != 0)
                        {
                            BIO_NADRA_CONFIGRATION edit = context.BIO_NADRA_CONFIGRATION.Where(m => m.NC_ID == C).FirstOrDefault();
                            if (edit != null)
                            {
                                BIO_NADRA_CONFIGRATION Update = edit;
                                Update.NC_FRANCHISE_ID = DAL.Decrypt(edit.NC_FRANCHISE_ID);
                                Update.NC_USER_NAME = DAL.Decrypt(edit.NC_USER_NAME);
                                Update.NC_PASSWORD = DAL.Decrypt(edit.NC_PASSWORD);
                                Update.NC_TEMPLATE_TYPE = DAL.Decrypt(edit.NC_TEMPLATE_TYPE);
                                return View(Update);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + C;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Verification", "Configration", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Configration(BIO_NADRA_CONFIGRATION db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Verification", "Configration", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        BIO_NADRA_CONFIGRATION Entity = context.BIO_NADRA_CONFIGRATION.Where(m => m.NC_ID == db_table.NC_ID).FirstOrDefault();
                        if (Entity != null)
                        {
                            Entity.NC_FRANCHISE_ID = DAL.Encrypt(db_table.NC_FRANCHISE_ID);
                            Entity.NC_USER_NAME = DAL.Encrypt(db_table.NC_USER_NAME);
                            Entity.NC_PASSWORD = DAL.Encrypt(db_table.NC_PASSWORD);
                            Entity.NC_TEMPLATE_TYPE = DAL.Encrypt(db_table.NC_TEMPLATE_TYPE);
                            context.Entry(Entity).State = System.Data.Entity.EntityState.Modified;
                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                message = "Data Updated Successfully " + RowsAffected + " Affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                            message = "Select record to update";
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Verification", "Configration", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult ConfigrationList()
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                List<BIO_NADRA_CONFIGRATION> Data = context.BIO_NADRA_CONFIGRATION.ToList();
                return PartialView(Data);
            }
            else
            {
                return PartialView(null);
            }
        }
        #endregion
    }
}