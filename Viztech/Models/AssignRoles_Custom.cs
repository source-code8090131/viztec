﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Viztech.Models
{
    public class AssignRoles_Custom
    {
        public BIO_LOGIN Entity { get; set; }
        public string[] SelectedRoles { get; set; }
        public IEnumerable<SelectListItem> RolesList { get; set; }

    }
}