﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Viztech.Models
{
    public class AssignFunctions_Custom
    {
        public BIO_ROLES Entity { get; set; }
        public string[] SelectedFunctions { get; set; }
        public IEnumerable<SelectListItem> FunctionsList { get; set; }
    }
}