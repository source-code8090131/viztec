//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Viztech.Models
{
    using System;
    
    public partial class SP_BIO_GET_GIDA_USER_ACTIVITY_ADMIN_Result
    {
        public string USERNAME { get; set; }
        public string Roles_Assign { get; set; }
        public string Added_by { get; set; }
        public string Activate_by { get; set; }
        public Nullable<System.DateTime> MakerTime { get; set; }
        public Nullable<System.DateTime> CheckerTime { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
    }
}
