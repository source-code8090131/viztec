﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Viztech.Models
{
    public class DAL
    {
        #region Encrypt N Decrypt
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        #endregion
        public static bool CheckFunctionValidity(string CName, string AName, string userID)
        {
            try
            {
                ViztecEntities context = new ViztecEntities();
                BIO_LOGIN LoginDetail = context.BIO_LOGIN.Where(m => m.USER_NAME == userID).FirstOrDefault();
                bool success = false;
                string FunctionName = context.BIO_FUNCTION.Where(m => m.F_CONTROLLER == CName && m.F_ACTION == AName).Select(m => m.F_NAME).FirstOrDefault();
                string Roles = context.BIO_LOGIN_ROLES.Where(m => m.ID == LoginDetail.ID).Select(m => m.GroupName).FirstOrDefault();
                string[] RoleNameFromRoleID = Roles.Split(',');
                foreach (string CheckRoles in RoleNameFromRoleID)
                {
                    string RolesName = context.BIO_ROLES.Where(m => m.Role_Name == CheckRoles).Select(m => m.Assign_Role_Details).FirstOrDefault();
                    string[] FunctionNameFromRoleID = RolesName.Split(',');
                    foreach (string CheckFunction in FunctionNameFromRoleID)
                    {
                        BIO_ROLES checkfunction = context.BIO_ROLES.Where(m => m.Role_Name == CheckRoles && CheckFunction.Contains(FunctionName)).FirstOrDefault();
                        if (checkfunction == null)
                        {
                            success = false;
                        }
                        else
                        {
                            success = true;
                            return success;
                        }
                    }
                }
                return success;
            }
            catch (Exception ex)
            {
                LogException("DAL", "CheckFunctionValidity",userID, ex);
                return false;
            }
        }
        public static string GetIP()
        {
            string Str = "";
            Str = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        public static string LogException(string controllername, string functionName, string userId, Exception excep)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "Viztec_logs\\";
                string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
                string complete_path = path + filename;
                if (System.IO.Directory.Exists(path))
                {
                    bool_icorelogs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(path);
                    bool_icorelogs = true;
                }

                if (bool_icorelogs)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(complete_path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                        {
                            writer.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                            writer.WriteLine("Class/Form/Report Name: " + controllername);
                            writer.WriteLine("Function Name: " + functionName);
                            writer.WriteLine("User ID: " + userId);
                            if (excep.InnerException != null)
                            {
                                writer.WriteLine("Inner Exception: " + excep.InnerException);
                                if(excep.InnerException.Message != null)
                                    writer.WriteLine("      Inner Exception Message : " + excep.InnerException.Message);

                                if (excep.InnerException.InnerException != null)
                                {
                                    writer.WriteLine("Inner Inner Exception: " + excep.InnerException.InnerException);
                                    if (excep.InnerException.InnerException.Message != null)
                                        writer.WriteLine("      Inner Inner Exception Message : " + excep.InnerException.InnerException.Message);
                                }
                            }
                            writer.WriteLine("Exception Description:");
                            writer.WriteLine(excep.Message);
                            writer.WriteLine("Exception Stack Trace:");
                            writer.WriteLine(excep.StackTrace);
                            writer.WriteLine("--------------------------------------------------------------- ");
                            writer.Close();
                        }
                    }
                }


                return "Some error occured, please contact software administrator.";

            }
            catch (Exception ex)
            {
                string message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return message;
            }
        }
        #region Check and log SQL connection
        public bool ValidateSqlConnection()
        {
            string message = "";
            try
            {
                System.Data.SqlClient.SqlConnection conn = GetSqlConnection();
                conn.Open();
                System.Threading.Thread.Sleep(5000);
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return false;
            }
        }
        public System.Data.SqlClient.SqlConnection GetSqlConnection()
        {
            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection();
            try
            {
                string Test = System.Configuration.ConfigurationManager.ConnectionStrings["ViztecEntities"].ConnectionString;
                var temp = Test.Split('"');
                foreach (var item in temp)
                {
                    if (item.Contains("data source="))
                        Test = item;
                }
                con.ConnectionString = Test;
            }
            catch (Exception ex)
            {
                string Message = "";
                if (ex is System.Data.SqlClient.SqlException)
                {
                    System.Data.SqlClient.SqlException exx = (System.Data.SqlClient.SqlException)ex;
                    for (int i = 0; i < exx.Errors.Count; i++)
                    {
                        Message += "Index #" + i + "\n" +
                            "Message: " + exx.Errors[i].Message + Environment.NewLine +
                            "LineNumber: " + exx.Errors[i].LineNumber + Environment.NewLine +
                            "Source: " + exx.Errors[i].Source + Environment.NewLine +
                            "Procedure: " + exx.Errors[i].Procedure + Environment.NewLine;
                    }
                    LogDatabaseConnection("Error On Connection : " + Message);
                }
                else
                {
                    LogException("","","",ex);
                }
            }
            return con;
        }
        public static string LogDatabaseConnection(string response)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "Viztec_logs\\";
                string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
                string complete_path = path + filename;
                if (System.IO.Directory.Exists(path))
                {
                    bool_icorelogs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(path);
                    bool_icorelogs = true;
                }

                if (bool_icorelogs)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(complete_path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                        {
                            writer.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                            writer.WriteLine(response);
                            writer.WriteLine("--------------------------------------------------------------- ");
                            writer.Close();
                        }
                    }
                }


                return "Some error occured, please contact software administrator.";

            }
            catch (Exception ex)
            {
                string message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return message;
            }
        }
        #endregion
    }
}