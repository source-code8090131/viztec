﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Viztech.Models
{
    public class VerificationData
    {
        public bool AzadKashmir { get; set; }
        public bool Balochistan { get; set; }
        public bool Fata { get; set; }
        public bool GilgitBaltistan { get; set; }
        public bool KhyberPakhtunkhwa { get; set; }
        public bool Punjab { get; set; }
        public bool Sindh { get; set; }
        public string CitizenNumber { get; set; }
        public string ContactNumber { get; set; }
        public string ServerData { get; set; }
        public bool RightThumb { get; set; }
        public bool RightIndex { get; set; }
        public bool RightMiddle { get; set; }
        public bool RightRing { get; set; }
        public bool RightLittle { get; set; }
        public bool LeftThumb { get; set; }
        public bool LeftIndex { get; set; }
        public bool LeftMiddle { get; set; }
        public bool LeftRing { get; set; }
        public bool LeftLittle { get; set; }
        public VerifiedClientFromNadra response { get; set; }
        public string ValidateTimer { get; set; }
    }
}