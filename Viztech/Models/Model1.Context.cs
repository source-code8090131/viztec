﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Viztech.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class ViztecEntities : DbContext
    {
        public ViztecEntities()
            : base("name=ViztecEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<BIO_FUNCTION> BIO_FUNCTION { get; set; }
        public virtual DbSet<BIO_LOGIN> BIO_LOGIN { get; set; }
        public virtual DbSet<BIO_LOGIN_ACTIVATION_LOG_TBL> BIO_LOGIN_ACTIVATION_LOG_TBL { get; set; }
        public virtual DbSet<BIO_LOGIN_ROLES> BIO_LOGIN_ROLES { get; set; }
        public virtual DbSet<BIO_NADRA_CONFIGRATION> BIO_NADRA_CONFIGRATION { get; set; }
        public virtual DbSet<BIO_ROLES> BIO_ROLES { get; set; }
        public virtual DbSet<BIO_ROLES_LOG_TBL> BIO_ROLES_LOG_TBL { get; set; }
        public virtual DbSet<BIOMETRIC_VERIFICATION_TBL> BIOMETRIC_VERIFICATION_TBL { get; set; }
        public virtual DbSet<DELETE_GROUP_LOG_TBL> DELETE_GROUP_LOG_TBL { get; set; }
        public virtual DbSet<GROUP_MODIFY_LOG_TBL> GROUP_MODIFY_LOG_TBL { get; set; }
        public virtual DbSet<user_login_logout_log> user_login_logout_log { get; set; }
        public virtual DbSet<USER_PROFILE_DETAILS> USER_PROFILE_DETAILS { get; set; }
        public virtual DbSet<User_TBL_ADD_LOG> User_TBL_ADD_LOG { get; set; }
        public virtual DbSet<USER_LOG_TBL> USER_LOG_TBL { get; set; }
        public virtual DbSet<BIO_LOGIN_VIEW> BIO_LOGIN_VIEW { get; set; }
        public virtual DbSet<BIO_SYSTEM_SETUP> BIO_SYSTEM_SETUP { get; set; }
        public virtual DbSet<BIO_REQUEST_LOG> BIO_REQUEST_LOG { get; set; }
    
        public virtual int BIO_GROUP_ROLES(string rOLE_NAME, string aSSIGN_ROLES, string rOLE_DESC, string aDDUSER, string iP, string activity, string aCTIVATOR_ID, string description)
        {
            var rOLE_NAMEParameter = rOLE_NAME != null ?
                new ObjectParameter("ROLE_NAME", rOLE_NAME) :
                new ObjectParameter("ROLE_NAME", typeof(string));
    
            var aSSIGN_ROLESParameter = aSSIGN_ROLES != null ?
                new ObjectParameter("ASSIGN_ROLES", aSSIGN_ROLES) :
                new ObjectParameter("ASSIGN_ROLES", typeof(string));
    
            var rOLE_DESCParameter = rOLE_DESC != null ?
                new ObjectParameter("ROLE_DESC", rOLE_DESC) :
                new ObjectParameter("ROLE_DESC", typeof(string));
    
            var aDDUSERParameter = aDDUSER != null ?
                new ObjectParameter("ADDUSER", aDDUSER) :
                new ObjectParameter("ADDUSER", typeof(string));
    
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            var activityParameter = activity != null ?
                new ObjectParameter("Activity", activity) :
                new ObjectParameter("Activity", typeof(string));
    
            var aCTIVATOR_IDParameter = aCTIVATOR_ID != null ?
                new ObjectParameter("ACTIVATOR_ID", aCTIVATOR_ID) :
                new ObjectParameter("ACTIVATOR_ID", typeof(string));
    
            var descriptionParameter = description != null ?
                new ObjectParameter("Description", description) :
                new ObjectParameter("Description", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("BIO_GROUP_ROLES", rOLE_NAMEParameter, aSSIGN_ROLESParameter, rOLE_DESCParameter, aDDUSERParameter, iPParameter, activityParameter, aCTIVATOR_IDParameter, descriptionParameter);
        }
    
        public virtual int GetCustomers_Pager_USER_LOG_TBL(string searchTerm, Nullable<int> pageIndex, Nullable<int> pageSize, ObjectParameter recordCount)
        {
            var searchTermParameter = searchTerm != null ?
                new ObjectParameter("SearchTerm", searchTerm) :
                new ObjectParameter("SearchTerm", typeof(string));
    
            var pageIndexParameter = pageIndex.HasValue ?
                new ObjectParameter("PageIndex", pageIndex) :
                new ObjectParameter("PageIndex", typeof(int));
    
            var pageSizeParameter = pageSize.HasValue ?
                new ObjectParameter("PageSize", pageSize) :
                new ObjectParameter("PageSize", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetCustomers_Pager_USER_LOG_TBL", searchTermParameter, pageIndexParameter, pageSizeParameter, recordCount);
        }
    
        public virtual int GetCustomers_Pager_user_login_logout_log(string searchTerm, Nullable<int> pageIndex, Nullable<int> pageSize, ObjectParameter recordCount)
        {
            var searchTermParameter = searchTerm != null ?
                new ObjectParameter("SearchTerm", searchTerm) :
                new ObjectParameter("SearchTerm", typeof(string));
    
            var pageIndexParameter = pageIndex.HasValue ?
                new ObjectParameter("PageIndex", pageIndex) :
                new ObjectParameter("PageIndex", typeof(int));
    
            var pageSizeParameter = pageSize.HasValue ?
                new ObjectParameter("PageSize", pageSize) :
                new ObjectParameter("PageSize", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetCustomers_Pager_user_login_logout_log", searchTermParameter, pageIndexParameter, pageSizeParameter, recordCount);
        }
    
        public virtual ObjectResult<Nullable<int>> SP_BIO_GET_ASSIGN_GROUPS(string group_Name)
        {
            var group_NameParameter = group_Name != null ?
                new ObjectParameter("Group_Name", group_Name) :
                new ObjectParameter("Group_Name", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SP_BIO_GET_ASSIGN_GROUPS", group_NameParameter);
        }
    
        public virtual ObjectResult<string> SP_BIO_GET_ASSIGN_ROLES(Nullable<int> bIO_LOGIN_ID)
        {
            var bIO_LOGIN_IDParameter = bIO_LOGIN_ID.HasValue ?
                new ObjectParameter("BIO_LOGIN_ID", bIO_LOGIN_ID) :
                new ObjectParameter("BIO_LOGIN_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("SP_BIO_GET_ASSIGN_ROLES", bIO_LOGIN_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_AUTHORIZE_USERS_Result> SP_BIO_GET_AUTHORIZE_USERS()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_AUTHORIZE_USERS_Result>("SP_BIO_GET_AUTHORIZE_USERS");
        }
    
        public virtual ObjectResult<SP_BIO_GET_AUTHORIZE_USERS_USER_Result> SP_BIO_GET_AUTHORIZE_USERS_USER(string uSER_ID)
        {
            var uSER_IDParameter = uSER_ID != null ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_AUTHORIZE_USERS_USER_Result>("SP_BIO_GET_AUTHORIZE_USERS_USER", uSER_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_BIO_ROLES_Result> SP_BIO_GET_BIO_ROLES()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_BIO_ROLES_Result>("SP_BIO_GET_BIO_ROLES");
        }
    
        public virtual ObjectResult<SP_BIO_GET_BIO_ROLES_ADMIN_Result> SP_BIO_GET_BIO_ROLES_ADMIN(Nullable<int> bIO_ROLE_ID)
        {
            var bIO_ROLE_IDParameter = bIO_ROLE_ID.HasValue ?
                new ObjectParameter("BIO_ROLE_ID", bIO_ROLE_ID) :
                new ObjectParameter("BIO_ROLE_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_BIO_ROLES_ADMIN_Result>("SP_BIO_GET_BIO_ROLES_ADMIN", bIO_ROLE_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_BIO_ROLES_ALL_Result> SP_BIO_GET_BIO_ROLES_ALL()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_BIO_ROLES_ALL_Result>("SP_BIO_GET_BIO_ROLES_ALL");
        }
    
        public virtual ObjectResult<SP_BIO_GET_BIOMETRIC_VERIFICATION_USER_Result> SP_BIO_GET_BIOMETRIC_VERIFICATION_USER(string cNIC)
        {
            var cNICParameter = cNIC != null ?
                new ObjectParameter("CNIC", cNIC) :
                new ObjectParameter("CNIC", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_BIOMETRIC_VERIFICATION_USER_Result>("SP_BIO_GET_BIOMETRIC_VERIFICATION_USER", cNICParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_DEACTIVATED_GROUPS_Result> SP_BIO_GET_DEACTIVATED_GROUPS()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_DEACTIVATED_GROUPS_Result>("SP_BIO_GET_DEACTIVATED_GROUPS");
        }
    
        public virtual ObjectResult<SP_BIO_GET_DEACTIVATED_GROUPS_USER_Result> SP_BIO_GET_DEACTIVATED_GROUPS_USER(string uSER_ID)
        {
            var uSER_IDParameter = uSER_ID != null ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_DEACTIVATED_GROUPS_USER_Result>("SP_BIO_GET_DEACTIVATED_GROUPS_USER", uSER_IDParameter);
        }
    
        public virtual int SP_BIO_GET_GIDA_CUSTOMIZE_REPORT_ADMIN(string dT_FROM, string dT_TO, string cols, string action)
        {
            var dT_FROMParameter = dT_FROM != null ?
                new ObjectParameter("DT_FROM", dT_FROM) :
                new ObjectParameter("DT_FROM", typeof(string));
    
            var dT_TOParameter = dT_TO != null ?
                new ObjectParameter("DT_TO", dT_TO) :
                new ObjectParameter("DT_TO", typeof(string));
    
            var colsParameter = cols != null ?
                new ObjectParameter("Cols", cols) :
                new ObjectParameter("Cols", typeof(string));
    
            var actionParameter = action != null ?
                new ObjectParameter("Action", action) :
                new ObjectParameter("Action", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_BIO_GET_GIDA_CUSTOMIZE_REPORT_ADMIN", dT_FROMParameter, dT_TOParameter, colsParameter, actionParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_GIDA_GROUP_DETAILS_ADMIN_Result> SP_BIO_GET_GIDA_GROUP_DETAILS_ADMIN(Nullable<System.DateTime> dT_FROM, Nullable<System.DateTime> dT_TO, string action)
        {
            var dT_FROMParameter = dT_FROM.HasValue ?
                new ObjectParameter("DT_FROM", dT_FROM) :
                new ObjectParameter("DT_FROM", typeof(System.DateTime));
    
            var dT_TOParameter = dT_TO.HasValue ?
                new ObjectParameter("DT_TO", dT_TO) :
                new ObjectParameter("DT_TO", typeof(System.DateTime));
    
            var actionParameter = action != null ?
                new ObjectParameter("Action", action) :
                new ObjectParameter("Action", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_GIDA_GROUP_DETAILS_ADMIN_Result>("SP_BIO_GET_GIDA_GROUP_DETAILS_ADMIN", dT_FROMParameter, dT_TOParameter, actionParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_GIDA_USER_ACTIVITY_ADMIN_Result> SP_BIO_GET_GIDA_USER_ACTIVITY_ADMIN(Nullable<System.DateTime> dT_FROM, Nullable<System.DateTime> dT_TO, string action)
        {
            var dT_FROMParameter = dT_FROM.HasValue ?
                new ObjectParameter("DT_FROM", dT_FROM) :
                new ObjectParameter("DT_FROM", typeof(System.DateTime));
    
            var dT_TOParameter = dT_TO.HasValue ?
                new ObjectParameter("DT_TO", dT_TO) :
                new ObjectParameter("DT_TO", typeof(System.DateTime));
    
            var actionParameter = action != null ?
                new ObjectParameter("Action", action) :
                new ObjectParameter("Action", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_GIDA_USER_ACTIVITY_ADMIN_Result>("SP_BIO_GET_GIDA_USER_ACTIVITY_ADMIN", dT_FROMParameter, dT_TOParameter, actionParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_GIDA_USER_DETAILS_ADMIN_Result> SP_BIO_GET_GIDA_USER_DETAILS_ADMIN(Nullable<System.DateTime> dT_FROM, Nullable<System.DateTime> dT_TO, string action)
        {
            var dT_FROMParameter = dT_FROM.HasValue ?
                new ObjectParameter("DT_FROM", dT_FROM) :
                new ObjectParameter("DT_FROM", typeof(System.DateTime));
    
            var dT_TOParameter = dT_TO.HasValue ?
                new ObjectParameter("DT_TO", dT_TO) :
                new ObjectParameter("DT_TO", typeof(System.DateTime));
    
            var actionParameter = action != null ?
                new ObjectParameter("Action", action) :
                new ObjectParameter("Action", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_GIDA_USER_DETAILS_ADMIN_Result>("SP_BIO_GET_GIDA_USER_DETAILS_ADMIN", dT_FROMParameter, dT_TOParameter, actionParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_GROUP_MODIFICATIONS_Result> SP_BIO_GET_GROUP_MODIFICATIONS()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_GROUP_MODIFICATIONS_Result>("SP_BIO_GET_GROUP_MODIFICATIONS");
        }
    
        public virtual ObjectResult<SP_BIO_GET_NON_AUTHORIZE_USERS_ADMIN_Result> SP_BIO_GET_NON_AUTHORIZE_USERS_ADMIN()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_NON_AUTHORIZE_USERS_ADMIN_Result>("SP_BIO_GET_NON_AUTHORIZE_USERS_ADMIN");
        }
    
        public virtual ObjectResult<SP_BIO_GET_NON_AUTHORIZE_USERS_USER_Result> SP_BIO_GET_NON_AUTHORIZE_USERS_USER(string aDD_USER)
        {
            var aDD_USERParameter = aDD_USER != null ?
                new ObjectParameter("ADD_USER", aDD_USER) :
                new ObjectParameter("ADD_USER", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_NON_AUTHORIZE_USERS_USER_Result>("SP_BIO_GET_NON_AUTHORIZE_USERS_USER", aDD_USERParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> SP_BIO_GET_ROLES_FOR_MODIFICATION(string role_Name)
        {
            var role_NameParameter = role_Name != null ?
                new ObjectParameter("Role_Name", role_Name) :
                new ObjectParameter("Role_Name", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SP_BIO_GET_ROLES_FOR_MODIFICATION", role_NameParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_AUTHORIZATION_Result> SP_BIO_GET_USER_AUTHORIZATION()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_AUTHORIZATION_Result>("SP_BIO_GET_USER_AUTHORIZATION");
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_DETAIL_LOGS_ADMIN_Result> SP_BIO_GET_USER_DETAIL_LOGS_ADMIN(Nullable<System.DateTime> dT_FROM, Nullable<System.DateTime> dT_TO, string cNIC, string action)
        {
            var dT_FROMParameter = dT_FROM.HasValue ?
                new ObjectParameter("DT_FROM", dT_FROM) :
                new ObjectParameter("DT_FROM", typeof(System.DateTime));
    
            var dT_TOParameter = dT_TO.HasValue ?
                new ObjectParameter("DT_TO", dT_TO) :
                new ObjectParameter("DT_TO", typeof(System.DateTime));
    
            var cNICParameter = cNIC != null ?
                new ObjectParameter("CNIC", cNIC) :
                new ObjectParameter("CNIC", typeof(string));
    
            var actionParameter = action != null ?
                new ObjectParameter("Action", action) :
                new ObjectParameter("Action", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_DETAIL_LOGS_ADMIN_Result>("SP_BIO_GET_USER_DETAIL_LOGS_ADMIN", dT_FROMParameter, dT_TOParameter, cNICParameter, actionParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_DETAILS_Result> SP_BIO_GET_USER_DETAILS(string uSER_ID)
        {
            var uSER_IDParameter = uSER_ID != null ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_DETAILS_Result>("SP_BIO_GET_USER_DETAILS", uSER_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_DETAILS_ADMIN_Result> SP_BIO_GET_USER_DETAILS_ADMIN(Nullable<int> uSER_ID)
        {
            var uSER_IDParameter = uSER_ID.HasValue ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_DETAILS_ADMIN_Result>("SP_BIO_GET_USER_DETAILS_ADMIN", uSER_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_DETAILS_CHECKER_Result> SP_BIO_GET_USER_DETAILS_CHECKER(Nullable<int> uSER_ID)
        {
            var uSER_IDParameter = uSER_ID.HasValue ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_DETAILS_CHECKER_Result>("SP_BIO_GET_USER_DETAILS_CHECKER", uSER_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_DETAILS_ROLES_Result> SP_BIO_GET_USER_DETAILS_ROLES(string lOGIN_ID)
        {
            var lOGIN_IDParameter = lOGIN_ID != null ?
                new ObjectParameter("LOGIN_ID", lOGIN_ID) :
                new ObjectParameter("LOGIN_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_DETAILS_ROLES_Result>("SP_BIO_GET_USER_DETAILS_ROLES", lOGIN_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_LOGIN_LOGS_ADMIN_Result> SP_BIO_GET_USER_LOGIN_LOGS_ADMIN(Nullable<System.DateTime> dT_FROM, Nullable<System.DateTime> dT_TO, string action)
        {
            var dT_FROMParameter = dT_FROM.HasValue ?
                new ObjectParameter("DT_FROM", dT_FROM) :
                new ObjectParameter("DT_FROM", typeof(System.DateTime));
    
            var dT_TOParameter = dT_TO.HasValue ?
                new ObjectParameter("DT_TO", dT_TO) :
                new ObjectParameter("DT_TO", typeof(System.DateTime));
    
            var actionParameter = action != null ?
                new ObjectParameter("Action", action) :
                new ObjectParameter("Action", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_LOGIN_LOGS_ADMIN_Result>("SP_BIO_GET_USER_LOGIN_LOGS_ADMIN", dT_FROMParameter, dT_TOParameter, actionParameter);
        }
    
        public virtual ObjectResult<SP_BIO_GET_USER_PROFILE_Result> SP_BIO_GET_USER_PROFILE(string uSER_ID)
        {
            var uSER_IDParameter = uSER_ID != null ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_GET_USER_PROFILE_Result>("SP_BIO_GET_USER_PROFILE", uSER_IDParameter);
        }
    
        public virtual ObjectResult<SP_BIO_LOGIN_DETAILS_Result> SP_BIO_LOGIN_DETAILS(string email, string password, string actionType)
        {
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("Password", password) :
                new ObjectParameter("Password", typeof(string));
    
            var actionTypeParameter = actionType != null ?
                new ObjectParameter("ActionType", actionType) :
                new ObjectParameter("ActionType", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_BIO_LOGIN_DETAILS_Result>("SP_BIO_LOGIN_DETAILS", emailParameter, passwordParameter, actionTypeParameter);
        }
    
        public virtual int SP_DELETE_BIO_ROLES(string gROUP_NAME)
        {
            var gROUP_NAMEParameter = gROUP_NAME != null ?
                new ObjectParameter("GROUP_NAME", gROUP_NAME) :
                new ObjectParameter("GROUP_NAME", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_DELETE_BIO_ROLES", gROUP_NAMEParameter);
        }
    
        public virtual int SP_GRP_DELETE_LOG(string gROUP_NAME, string uSERID, string iP)
        {
            var gROUP_NAMEParameter = gROUP_NAME != null ?
                new ObjectParameter("GROUP_NAME", gROUP_NAME) :
                new ObjectParameter("GROUP_NAME", typeof(string));
    
            var uSERIDParameter = uSERID != null ?
                new ObjectParameter("USERID", uSERID) :
                new ObjectParameter("USERID", typeof(string));
    
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GRP_DELETE_LOG", gROUP_NAMEParameter, uSERIDParameter, iPParameter);
        }
    
        public virtual int SP_UPDATE_AUTHORIZE_USER(Nullable<int> iD, string uSER_NAME, string eMAIL, string tYPE, string uSER_STATUS, string aCTIVATOR_ID, string iP, string roles_Assign, string groupName, string activity, string description, Nullable<System.DateTime> makerTime, Nullable<System.DateTime> checkerTime, string addedby, string activateby)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var uSER_NAMEParameter = uSER_NAME != null ?
                new ObjectParameter("USER_NAME", uSER_NAME) :
                new ObjectParameter("USER_NAME", typeof(string));
    
            var eMAILParameter = eMAIL != null ?
                new ObjectParameter("EMAIL", eMAIL) :
                new ObjectParameter("EMAIL", typeof(string));
    
            var tYPEParameter = tYPE != null ?
                new ObjectParameter("TYPE", tYPE) :
                new ObjectParameter("TYPE", typeof(string));
    
            var uSER_STATUSParameter = uSER_STATUS != null ?
                new ObjectParameter("USER_STATUS", uSER_STATUS) :
                new ObjectParameter("USER_STATUS", typeof(string));
    
            var aCTIVATOR_IDParameter = aCTIVATOR_ID != null ?
                new ObjectParameter("ACTIVATOR_ID", aCTIVATOR_ID) :
                new ObjectParameter("ACTIVATOR_ID", typeof(string));
    
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            var roles_AssignParameter = roles_Assign != null ?
                new ObjectParameter("Roles_Assign", roles_Assign) :
                new ObjectParameter("Roles_Assign", typeof(string));
    
            var groupNameParameter = groupName != null ?
                new ObjectParameter("GroupName", groupName) :
                new ObjectParameter("GroupName", typeof(string));
    
            var activityParameter = activity != null ?
                new ObjectParameter("Activity", activity) :
                new ObjectParameter("Activity", typeof(string));
    
            var descriptionParameter = description != null ?
                new ObjectParameter("Description", description) :
                new ObjectParameter("Description", typeof(string));
    
            var makerTimeParameter = makerTime.HasValue ?
                new ObjectParameter("MakerTime", makerTime) :
                new ObjectParameter("MakerTime", typeof(System.DateTime));
    
            var checkerTimeParameter = checkerTime.HasValue ?
                new ObjectParameter("CheckerTime", checkerTime) :
                new ObjectParameter("CheckerTime", typeof(System.DateTime));
    
            var addedbyParameter = addedby != null ?
                new ObjectParameter("Addedby", addedby) :
                new ObjectParameter("Addedby", typeof(string));
    
            var activatebyParameter = activateby != null ?
                new ObjectParameter("Activateby", activateby) :
                new ObjectParameter("Activateby", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_UPDATE_AUTHORIZE_USER", iDParameter, uSER_NAMEParameter, eMAILParameter, tYPEParameter, uSER_STATUSParameter, aCTIVATOR_IDParameter, iPParameter, roles_AssignParameter, groupNameParameter, activityParameter, descriptionParameter, makerTimeParameter, checkerTimeParameter, addedbyParameter, activatebyParameter);
        }
    
        public virtual int SP_UPDATE_BIO_LOGIN_ADD_USER(Nullable<int> uSER_ID, string aDD_USER)
        {
            var uSER_IDParameter = uSER_ID.HasValue ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(int));
    
            var aDD_USERParameter = aDD_USER != null ?
                new ObjectParameter("ADD_USER", aDD_USER) :
                new ObjectParameter("ADD_USER", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_UPDATE_BIO_LOGIN_ADD_USER", uSER_IDParameter, aDD_USERParameter);
        }
    
        public virtual int SP_UPDATE_BIO_LOGIN_ADD_USER_ADMIN(Nullable<int> uSER_ID, string aDD_USER)
        {
            var uSER_IDParameter = uSER_ID.HasValue ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(int));
    
            var aDD_USERParameter = aDD_USER != null ?
                new ObjectParameter("ADD_USER", aDD_USER) :
                new ObjectParameter("ADD_USER", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_UPDATE_BIO_LOGIN_ADD_USER_ADMIN", uSER_IDParameter, aDD_USERParameter);
        }
    
        public virtual int SP_UPDATE_BIO_ROLES(Nullable<int> role_ID, string rOLE_NAME, string aSSIGN_ROLES, string rOLE_DESC, string aDDUSER, string iP, string activity, string aCTIVATOR_ID, string gROUP_STATUS, string description)
        {
            var role_IDParameter = role_ID.HasValue ?
                new ObjectParameter("Role_ID", role_ID) :
                new ObjectParameter("Role_ID", typeof(int));
    
            var rOLE_NAMEParameter = rOLE_NAME != null ?
                new ObjectParameter("ROLE_NAME", rOLE_NAME) :
                new ObjectParameter("ROLE_NAME", typeof(string));
    
            var aSSIGN_ROLESParameter = aSSIGN_ROLES != null ?
                new ObjectParameter("ASSIGN_ROLES", aSSIGN_ROLES) :
                new ObjectParameter("ASSIGN_ROLES", typeof(string));
    
            var rOLE_DESCParameter = rOLE_DESC != null ?
                new ObjectParameter("ROLE_DESC", rOLE_DESC) :
                new ObjectParameter("ROLE_DESC", typeof(string));
    
            var aDDUSERParameter = aDDUSER != null ?
                new ObjectParameter("ADDUSER", aDDUSER) :
                new ObjectParameter("ADDUSER", typeof(string));
    
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            var activityParameter = activity != null ?
                new ObjectParameter("Activity", activity) :
                new ObjectParameter("Activity", typeof(string));
    
            var aCTIVATOR_IDParameter = aCTIVATOR_ID != null ?
                new ObjectParameter("ACTIVATOR_ID", aCTIVATOR_ID) :
                new ObjectParameter("ACTIVATOR_ID", typeof(string));
    
            var gROUP_STATUSParameter = gROUP_STATUS != null ?
                new ObjectParameter("GROUP_STATUS", gROUP_STATUS) :
                new ObjectParameter("GROUP_STATUS", typeof(string));
    
            var descriptionParameter = description != null ?
                new ObjectParameter("Description", description) :
                new ObjectParameter("Description", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_UPDATE_BIO_ROLES", role_IDParameter, rOLE_NAMEParameter, aSSIGN_ROLESParameter, rOLE_DESCParameter, aDDUSERParameter, iPParameter, activityParameter, aCTIVATOR_IDParameter, gROUP_STATUSParameter, descriptionParameter);
        }
    
        public virtual int SP_UPDATE_BIO_ROLES_ADD_USER(Nullable<int> bIO_ROLE_ID, string aDD_USER)
        {
            var bIO_ROLE_IDParameter = bIO_ROLE_ID.HasValue ?
                new ObjectParameter("BIO_ROLE_ID", bIO_ROLE_ID) :
                new ObjectParameter("BIO_ROLE_ID", typeof(int));
    
            var aDD_USERParameter = aDD_USER != null ?
                new ObjectParameter("ADD_USER", aDD_USER) :
                new ObjectParameter("ADD_USER", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_UPDATE_BIO_ROLES_ADD_USER", bIO_ROLE_IDParameter, aDD_USERParameter);
        }
    
        public virtual int SP_UPDATE_GROUP(string gROUP_STATUS, string bIO_ROLE_ID, string group_Name, string aCTIVATOR_ID, string cURRENT_STATUS, string activity, string activaorID)
        {
            var gROUP_STATUSParameter = gROUP_STATUS != null ?
                new ObjectParameter("GROUP_STATUS", gROUP_STATUS) :
                new ObjectParameter("GROUP_STATUS", typeof(string));
    
            var bIO_ROLE_IDParameter = bIO_ROLE_ID != null ?
                new ObjectParameter("BIO_ROLE_ID", bIO_ROLE_ID) :
                new ObjectParameter("BIO_ROLE_ID", typeof(string));
    
            var group_NameParameter = group_Name != null ?
                new ObjectParameter("Group_Name", group_Name) :
                new ObjectParameter("Group_Name", typeof(string));
    
            var aCTIVATOR_IDParameter = aCTIVATOR_ID != null ?
                new ObjectParameter("ACTIVATOR_ID", aCTIVATOR_ID) :
                new ObjectParameter("ACTIVATOR_ID", typeof(string));
    
            var cURRENT_STATUSParameter = cURRENT_STATUS != null ?
                new ObjectParameter("CURRENT_STATUS", cURRENT_STATUS) :
                new ObjectParameter("CURRENT_STATUS", typeof(string));
    
            var activityParameter = activity != null ?
                new ObjectParameter("Activity", activity) :
                new ObjectParameter("Activity", typeof(string));
    
            var activaorIDParameter = activaorID != null ?
                new ObjectParameter("ActivaorID", activaorID) :
                new ObjectParameter("ActivaorID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_UPDATE_GROUP", gROUP_STATUSParameter, bIO_ROLE_IDParameter, group_NameParameter, aCTIVATOR_IDParameter, cURRENT_STATUSParameter, activityParameter, activaorIDParameter);
        }
    
        public virtual int SP_UPDATE_USER_PASSWORD(string uSER_ID, string pASSWORD)
        {
            var uSER_IDParameter = uSER_ID != null ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(string));
    
            var pASSWORDParameter = pASSWORD != null ?
                new ObjectParameter("PASSWORD", pASSWORD) :
                new ObjectParameter("PASSWORD", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_UPDATE_USER_PASSWORD", uSER_IDParameter, pASSWORDParameter);
        }
    
        public virtual int SP_USER_NADRA_DETAIL_LOG(string cNIC, string cITIZEN_NAME, string fATHER_HUSBAND_NAME, string bIRTH_PLACE, string dATE_OF_BIRTH, string cNIC_EXPIRY_DATE, string pHOTOGRAPH, string pRESENT_ADDRESS, string pERMANENT_ADDRESS, string iP)
        {
            var cNICParameter = cNIC != null ?
                new ObjectParameter("CNIC", cNIC) :
                new ObjectParameter("CNIC", typeof(string));
    
            var cITIZEN_NAMEParameter = cITIZEN_NAME != null ?
                new ObjectParameter("CITIZEN_NAME", cITIZEN_NAME) :
                new ObjectParameter("CITIZEN_NAME", typeof(string));
    
            var fATHER_HUSBAND_NAMEParameter = fATHER_HUSBAND_NAME != null ?
                new ObjectParameter("FATHER_HUSBAND_NAME", fATHER_HUSBAND_NAME) :
                new ObjectParameter("FATHER_HUSBAND_NAME", typeof(string));
    
            var bIRTH_PLACEParameter = bIRTH_PLACE != null ?
                new ObjectParameter("BIRTH_PLACE", bIRTH_PLACE) :
                new ObjectParameter("BIRTH_PLACE", typeof(string));
    
            var dATE_OF_BIRTHParameter = dATE_OF_BIRTH != null ?
                new ObjectParameter("DATE_OF_BIRTH", dATE_OF_BIRTH) :
                new ObjectParameter("DATE_OF_BIRTH", typeof(string));
    
            var cNIC_EXPIRY_DATEParameter = cNIC_EXPIRY_DATE != null ?
                new ObjectParameter("CNIC_EXPIRY_DATE", cNIC_EXPIRY_DATE) :
                new ObjectParameter("CNIC_EXPIRY_DATE", typeof(string));
    
            var pHOTOGRAPHParameter = pHOTOGRAPH != null ?
                new ObjectParameter("PHOTOGRAPH", pHOTOGRAPH) :
                new ObjectParameter("PHOTOGRAPH", typeof(string));
    
            var pRESENT_ADDRESSParameter = pRESENT_ADDRESS != null ?
                new ObjectParameter("PRESENT_ADDRESS", pRESENT_ADDRESS) :
                new ObjectParameter("PRESENT_ADDRESS", typeof(string));
    
            var pERMANENT_ADDRESSParameter = pERMANENT_ADDRESS != null ?
                new ObjectParameter("PERMANENT_ADDRESS", pERMANENT_ADDRESS) :
                new ObjectParameter("PERMANENT_ADDRESS", typeof(string));
    
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_USER_NADRA_DETAIL_LOG", cNICParameter, cITIZEN_NAMEParameter, fATHER_HUSBAND_NAMEParameter, bIRTH_PLACEParameter, dATE_OF_BIRTHParameter, cNIC_EXPIRY_DATEParameter, pHOTOGRAPHParameter, pRESENT_ADDRESSParameter, pERMANENT_ADDRESSParameter, iPParameter);
        }
    
        public virtual int SP_USER_PROFILE_DETAILS(string uSER_ID, string uSER_NAME, string uSER_DESIGNATION, string uSER_IMAGE)
        {
            var uSER_IDParameter = uSER_ID != null ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(string));
    
            var uSER_NAMEParameter = uSER_NAME != null ?
                new ObjectParameter("USER_NAME", uSER_NAME) :
                new ObjectParameter("USER_NAME", typeof(string));
    
            var uSER_DESIGNATIONParameter = uSER_DESIGNATION != null ?
                new ObjectParameter("USER_DESIGNATION", uSER_DESIGNATION) :
                new ObjectParameter("USER_DESIGNATION", typeof(string));
    
            var uSER_IMAGEParameter = uSER_IMAGE != null ?
                new ObjectParameter("USER_IMAGE", uSER_IMAGE) :
                new ObjectParameter("USER_IMAGE", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_USER_PROFILE_DETAILS", uSER_IDParameter, uSER_NAMEParameter, uSER_DESIGNATIONParameter, uSER_IMAGEParameter);
        }
    }
}
