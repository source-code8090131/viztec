﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Viztech.Models
{
    public class CUSTOM_LOGIN
    {
        public int ID { get; set; }
        public string USER_NAME { get; set; }
        public string EMAIL { get; set; }
        public string PASSWORD { get; set; }
        public string TYPE { get; set; }
        public string USER_STATUS { get; set; }
        public string AddUser { get; set; }
        public string USER_ID { get; set; }
        public string Activity { get; set; }
        public string Activator_ID { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
        public string DOMAIN_NAME { get; set; }
    }
}