﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Viztech.Models
{
    public class VerifiedClientFromNadra
    {
        public string NAME { get; set; }
        public string FATHER_OR_HUSBAND_NAME { get; set; }
        public string PRESENT_ADDRESS { get; set; }
        public string CNIC_NO { get; set; }
        public string PERMANANT_ADDRESS { get; set; }
        public string DATE_OF_BIRTH { get; set; }
        public string BIRTH_PLACE { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string IMAGE_URL { get; set; }
        public string ACTIVITY_DATE { get; set; }
    }
}