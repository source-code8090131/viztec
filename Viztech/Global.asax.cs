﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viztech.Models;
using System.Web.Routing;

namespace Viztech
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            string message = "";
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "Viztec_logs\\";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
            string FullPath = path + filename;
            string complete_path = path + filename;
            if (System.IO.File.Exists(FullPath))
            {
                bool_icorelogs = true;
            }
            else
            {
                DAL dal = new DAL();
                if (dal.ValidateSqlConnection())
                {
                    message = "Attemp To Connect With Database Was Successful." + DateTime.Now;

                }
                else
                {
                    message = "Attemp To Connect With Database Was Failed." + DateTime.Now;
                }
                DAL.LogDatabaseConnection(message);
                bool_icorelogs = true;
            }
        }
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            string[] headers = { "Server", "X-AspNet-Version", "X-AspNetMvc-Version", "X-Powered-By" };

            if (!Response.HeadersWritten)
            {
                Response.AddOnSendingHeaders((c) =>
                {
                    if (c != null && c.Response != null && c.Response.Headers != null)
                    {
                        foreach (string header in headers)
                        {
                            if (c.Response.Headers[header] != null)
                            {
                                c.Response.Headers.Remove(header);
                            }
                        }
                    }
                });
            }
            string message = "";
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "Viztec_logs\\";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
            string FullPath = path + filename;
            string complete_path = path + filename;
            if (System.IO.File.Exists(FullPath))
            {
                bool_icorelogs = true;
            }
            else
            {
                DAL dal = new DAL();
                if (dal.ValidateSqlConnection())
                {
                    message = "Attemp To Connect With Database Was Successful." + DateTime.Now;

                }
                else
                {
                    message = "Attemp To Connect With Database Was Failed." + DateTime.Now;
                }
                DAL.LogDatabaseConnection(message);
                bool_icorelogs = true;
            }
        }
    }
}
