﻿//#region Custom Template Js
$(window).on('load', function () {
    $('#loading').hide();

    if ($("#CustomModal").length > 0) {
        if ($("#CustomModal").val() != null && $("#CustomModal").val() != "") {
            $('#modal-xl').modal('show');
        }
    }
});
GetSessionTime();
//if (window.location.href.toString().includes("/Home")) {
//    GetSessionTime();
//}
//#region Redirect to Logout after Session Expire

$("#ContactNumber").keypress(function (button) {
    return DecimalsOnly(event);
});
$("#CitizenNumber").keypress(function (button) {
    return DecimalsOnly(event);
});

function DecimalsOnly(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function GetSessionTime() {
    $.ajax
        ({
            url: '/Home/SessionTime',
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                var Timeout = data.SessionTime;
                Timeout = Timeout * 60000;
                //alert("Timeout : " + Timeout);
                SetTimeoutFunction(Timeout);
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
}
function SetTimeoutFunction(TimeinSec) {
    setTimeout(function () {
        location.href = '/Login/Index';
    }, TimeinSec);
}
//#endregion

//#region Expire Session upon browser tab closer
$(window).on('mouseover', (function () {
    window.onbeforeunload = null;
}));
$(window).on('mouseout', (function () {
    window.onbeforeunload = ConfirmLeave;
}));
function ConfirmLeave() {
    $.ajax
        ({
            url: '/Login/Logout',
            type: 'GET',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
        });
}
var prevKey = "";
$(document).keydown(function (e) {
    if (e.key == "F5") {
        return "";
    }
    else if (e.key.toUpperCase() == "W" && prevKey == "CONTROL") {
        window.onbeforeunload = ConfirmLeave;
    }
    else if (e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
        return "";
    }
    else if (e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
        return "";
    }
    prevKey = e.key.toUpperCase();
});
//#endregion

$(function () {
    $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#Reporting").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#Reporting_wrapper .col-md-6:eq(0)');

});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $(".closemodal").click(function (button) {
        $('#modal-xl').modal('hide');
    });

    $("#PrintModal").click(function (button) {
        var printContents = document.getElementById('ModalContent').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
        window.location.href = "/Verification/Biometric";
    });
});

//#endregion

//#region Group Role Active/De-active
$("[name='ActivateGroup']").click(function (button) {
    var R_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Roles/ActivateGroupRole?R_ID=' + R_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='DeactivateGroup']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to deactive?");
    if (check == true) {
        $.ajax
            ({
                url: '/Roles/DeactivateGroupRole?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region User Active/De-active
$("[name='ActivateRole']").click(function (button) {
    var ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Users/ActivateNonAuthorizeUser?R_ID=' + ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});

$("[name='DeactivateRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to deactive?");
    if (check == true) {
        $.ajax
            ({
                url: '/Users/DeactivateAuthorizeUser?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region Get Finger Print

$("[name='CallSGIFPGetData']").click(function (button) {
    CallSGIFPGetData(SuccessFunc1, ErrorFunc);
});


var secugen_lic = "";

function ErrorCodeToString(ErrorCode) {
    var Description;
    switch (ErrorCode) {
        // 0 - 999 - Comes from SgFplib.h
        // 1,000 - 9,999 - SGIBioSrv errors
        // 10,000 - 99,999 license errors
        case 51:
            Description = "System file load failure";
            break;
        case 52:
            Description = "Sensor chip initialization failed";
            break;
        case 53:
            Description = "Device not found";
            break;
        case 54:
            Description = "Fingerprint image capture timeout";
            break;
        case 55:
            Description = "No device available";
            break;
        case 56:
            Description = "Driver load failed";
            break;
        case 57:
            Description = "Wrong Image";
            break;
        case 58:
            Description = "Lack of bandwidth";
            break;
        case 59:
            Description = "Device Busy";
            break;
        case 60:
            Description = "Cannot get serial number of the device";
            break;
        case 61:
            Description = "Unsupported device";
            break;
        case 63:
            Description = "SgiBioSrv didn't start; Try image capture again";
            break;
        default:
            Description = "Unknown error code or Update code to reflect latest result" + ErrorCode;
            break;
    }
    return Description;
}

var templates = [];
var template_2 = "";
var count = 0;

var TempforServer = "";
function SuccessFunc1(result) {
    if (result.ErrorCode == 0) {
        /* 	Display BMP data in image tag
            BMP data is in base 64 format
        */
        if (result != null && result.BMPBase64.length > 0) {
            //< !--alert(result.TemplateBase64); --> /*(result.BMPBase64);*/
            TempforServer = result.TemplateBase64; /*result.BMPBase64;*/
            $("#ServerData").val(TempforServer);
            SetSession();

            document.getElementById('FPImage1').src = "data:image/bmp;base64," + result.BMPBase64;
        }
        templates[count] = result.TemplateBase64;
        count = count + 1;
    }
    else {
        alert("Fingerprint Capture Error Code:  " + result.ErrorCode + ".\nDescription:  " + ErrorCodeToString(result.ErrorCode) + ".");
    }
}

function SuccessFunc2(result) {
    if (result.ErrorCode == 0) {
        /* 	Display BMP data in image tag
            BMP data is in base 64 format
        */
        if (result != null && result.BMPBase64.length > 0) {
            document.getElementById('FPImage2').src = "data:image/bmp;base64," + result.BMPBase64;
        }
        template_2 = result.TemplateBase64;
    }
    else {
        alert("Fingerprint Capture Error Code:  " + result.ErrorCode + ".\nDescription:  " + ErrorCodeToString(result.ErrorCode) + ".");
    }
}

function ErrorFunc(status) {
    /*
        If you reach here, user is probabaly not running the
        service. Redirect the user to a page where he can download the
        executable and install it.
    */
    alert("Check if SGIBIOSRV is running; status = " + status + ":");
}

function CallSGIFPGetData(successCall, failCall) {
    //var curdate = new Date();
    //document.getElementById("ContentPlaceHolder1_txtActivityDateTime").value = curdate.toString();
    var uri = "https://localhost:8443/SGIFPCapture";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            fpobject = JSON.parse(xmlhttp.responseText);
            successCall(fpobject);
        }
        else if (xmlhttp.status == 404) {
            failCall(xmlhttp.status)
        }
    }
    xmlhttp.onerror = function () {
        failCall(xmlhttp.status);
    }
    var params = "Timeout=" + "10000";
    params += "&Quality=" + "50";
    params += "&licstr=" + encodeURIComponent(secugen_lic);
    params += "&templateFormat=" + "ISO";
    xmlhttp.open("POST", uri, true);
    xmlhttp.send(params);
}

function matchScore(succFunction, failFunction) {
    //if (template_1 == "" || template_2 == "") {
    //    alert("Please scan two fingers to verify!!");
    //    return;
    //}

    for (var i = 0; i < templates.length; i++) {

        var uri = "https://localhost:8443/SGIMatchScore";
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                fpobject = JSON.parse(xmlhttp.responseText);
                succFunction(fpobject, i);
            }
            else if (xmlhttp.status == 404) {
                failFunction(xmlhttp.status)
            }
        }

        xmlhttp.onerror = function () {
            failFunction(xmlhttp.status);
        }
        var params = "template1=" + encodeURIComponent(template_2);
        params += "&template2=" + encodeURIComponent(templates[i]);
        params += "&licstr=" + encodeURIComponent(secugen_lic);
        params += "&templateFormat=" + "ISO";
        //xmlhttp.open("POST", uri, false);
        xmlhttp.send(params);
    }
}

function succMatch(result, index) {
    var idQuality = document.getElementById("quality").value;
    if (result.ErrorCode == 0) {
        if (result.MatchingScore >= idQuality)
            alert("MATCHED ! (" + result.MatchingScore + ") at Index Person : " + index);
        //else
        //    alert("NOT MATCHED ! (" + result.MatchingScore + ")");
    }
    else {
        alert("Error Scanning Fingerprint ErrorCode = " + result.ErrorCode);
    }
}

function failureFunc(error) {
    alert("On Match Process, failure has been called");
}


function SetSession() {


    var value = TempforServer;
    var arr = { value: value };

    $.ajax({
        type: "POST",
        url: "/Verification/SetSession",
        async: false,
        data: JSON.stringify(arr),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: OnFailure,
        Error: Error
    });
}


function OnSuccess(response) {
    //alert(response.d);
    //alert("Ajax Call Successfull");

}
function OnFailure(response) {
    //alert("Ajax Call Failed");
}

function Error(response) {
    //alert("Ajax Call Error");
}


//#endregion

//#region Send Request To NADRA
$("#btnrequest").click(function (button) {
    var CitizenNo = $("#CitizenNumber").val();
    var ContactNo = $("#ContactNumber").val();
    alert("Citizen Number : " + CitizenNo + " Contact Number : " + ContactNo);
    //$.ajax
    //    ({
    //        url: '/Verification/Biometric',
    //        type: 'POST',
    //        datatype: 'application/json',
    //        contentType: 'application/json',
    //        beforeSend: function () {
    //            $('#loading').show();
    //        },
    //        complete: function () {
    //            $('#loading').hide();
    //        },
    //        success: function () {
    //        },
    //        error: function () {
    //            //alert("Something went wrong..");
    //        },
    //    });
});
//#endregion

//#region Browser Back Button
$("[name='BrowserBack']").click(function (button) {
    history.back();
});
//#endregion

//#region Timer Code
//if ($('#CountdownTimer').length) {
//    document.getElementById("SendRequest").disabled = true;
//    var seconds = 30;
//    var timer;
//    function myFunction() {
//        if (seconds < 30) {
//            document.getElementById("CountdownTimer").innerHTML = seconds;
//        }
//        if (seconds > 0) {
//            seconds--;
//        } else {
//            clearInterval(timer);
//            document.getElementById("SendRequest").disabled = false;
//        }
//    }
//    if (!timer) {
//        timer = window.setInterval(function () {
//            myFunction();
//        }, 1000);
//    }
    
//    document.getElementById("CountdownTimer").innerHTML = "30"; 
    
//}

//#endregion

//#region Enable/Disable Region Checkbox
if (document.getElementById('AzadKashmir').checked) {
    $('#Balochistan').attr('disabled', 'disabled');
    $('#Fata').attr('disabled', 'disabled');
    $('#GilgitBaltistan').attr('disabled', 'disabled');
    $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
    $('#Punjab').attr('disabled', 'disabled');
    $('#Sindh').attr('disabled', 'disabled');
}
if (document.getElementById('Balochistan').checked) {
    $('#AzadKashmir').attr('disabled', 'disabled');
    $('#Fata').attr('disabled', 'disabled');
    $('#GilgitBaltistan').attr('disabled', 'disabled');
    $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
    $('#Punjab').attr('disabled', 'disabled');
    $('#Sindh').attr('disabled', 'disabled');
}
if (document.getElementById('Fata').checked) {
    $('#Balochistan').attr('disabled', 'disabled');
    $('#AzadKashmir').attr('disabled', 'disabled');
    $('#GilgitBaltistan').attr('disabled', 'disabled');
    $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
    $('#Punjab').attr('disabled', 'disabled');
    $('#Sindh').attr('disabled', 'disabled');
}
if (document.getElementById('GilgitBaltistan').checked) {
    $('#Balochistan').attr('disabled', 'disabled');
    $('#Fata').attr('disabled', 'disabled');
    $('#AzadKashmir').attr('disabled', 'disabled');
    $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
    $('#Punjab').attr('disabled', 'disabled');
    $('#Sindh').attr('disabled', 'disabled');
}
if (document.getElementById('KhyberPakhtunkhwa').checked) {
    $('#Balochistan').attr('disabled', 'disabled');
    $('#Fata').attr('disabled', 'disabled');
    $('#GilgitBaltistan').attr('disabled', 'disabled');
    $('#AzadKashmir').attr('disabled', 'disabled');
    $('#Punjab').attr('disabled', 'disabled');
    $('#Sindh').attr('disabled', 'disabled');
}
if (document.getElementById('Punjab').checked) {
    $('#Balochistan').attr('disabled', 'disabled');
    $('#Fata').attr('disabled', 'disabled');
    $('#GilgitBaltistan').attr('disabled', 'disabled');
    $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
    $('#AzadKashmir').attr('disabled', 'disabled');
    $('#Sindh').attr('disabled', 'disabled');
}
if (document.getElementById('Sindh').checked) {
    $('#Balochistan').attr('disabled', 'disabled');
    $('#Fata').attr('disabled', 'disabled');
    $('#GilgitBaltistan').attr('disabled', 'disabled');
    $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
    $('#Punjab').attr('disabled', 'disabled');
    $('#AzadKashmir').attr('disabled', 'disabled');
}

$("#AzadKashmir").change(function () {
    if ($(this).prop('checked')) {
        $('#Balochistan').attr('disabled', 'disabled');
        $('#Fata').attr('disabled', 'disabled');
        $('#GilgitBaltistan').attr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
        $('#Punjab').attr('disabled', 'disabled');
        $('#Sindh').attr('disabled', 'disabled');
    } else {
        $('#Balochistan').removeAttr('disabled', 'disabled');
        $('#Fata').removeAttr('disabled', 'disabled');
        $('#GilgitBaltistan').removeAttr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').removeAttr('disabled', 'disabled');
        $('#Punjab').removeAttr('disabled', 'disabled');
        $('#Sindh').removeAttr('disabled', 'disabled');
    }
});

$("#Balochistan").change(function () {
    if ($(this).prop('checked')) {
        $('#AzadKashmir').attr('disabled', 'disabled');
        $('#Fata').attr('disabled', 'disabled');
        $('#GilgitBaltistan').attr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
        $('#Punjab').attr('disabled', 'disabled');
        $('#Sindh').attr('disabled', 'disabled');
    } else {
        $('#AzadKashmir').removeAttr('disabled', 'disabled');
        $('#Fata').removeAttr('disabled', 'disabled');
        $('#GilgitBaltistan').removeAttr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').removeAttr('disabled', 'disabled');
        $('#Punjab').removeAttr('disabled', 'disabled');
        $('#Sindh').removeAttr('disabled', 'disabled');
    }
});
$("#Fata").change(function () {
    if ($(this).prop('checked')) {
        $('#AzadKashmir').attr('disabled', 'disabled');
        $('#Balochistan').attr('disabled', 'disabled');
        $('#GilgitBaltistan').attr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
        $('#Punjab').attr('disabled', 'disabled');
        $('#Sindh').attr('disabled', 'disabled');
    } else {
        $('#AzadKashmir').removeAttr('disabled', 'disabled');
        $('#Balochistan').removeAttr('disabled', 'disabled');
        $('#GilgitBaltistan').removeAttr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').removeAttr('disabled', 'disabled');
        $('#Punjab').removeAttr('disabled', 'disabled');
        $('#Sindh').removeAttr('disabled', 'disabled');
    }
});
$("#GilgitBaltistan").change(function () {
    if ($(this).prop('checked')) {
        $('#AzadKashmir').attr('disabled', 'disabled');
        $('#Balochistan').attr('disabled', 'disabled');
        $('#Fata').attr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
        $('#Punjab').attr('disabled', 'disabled');
        $('#Sindh').attr('disabled', 'disabled');
    } else {
        $('#AzadKashmir').removeAttr('disabled', 'disabled');
        $('#Balochistan').removeAttr('disabled', 'disabled');
        $('#Fata').removeAttr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').removeAttr('disabled', 'disabled');
        $('#Punjab').removeAttr('disabled', 'disabled');
        $('#Sindh').removeAttr('disabled', 'disabled');
    }
});
$("#KhyberPakhtunkhwa").change(function () {
    if ($(this).prop('checked')) {
        $('#AzadKashmir').attr('disabled', 'disabled');
        $('#Balochistan').attr('disabled', 'disabled');
        $('#Fata').attr('disabled', 'disabled');
        $('#GilgitBaltistan').attr('disabled', 'disabled');
        $('#Punjab').attr('disabled', 'disabled');
        $('#Sindh').attr('disabled', 'disabled');
    } else {
        $('#AzadKashmir').removeAttr('disabled', 'disabled');
        $('#Balochistan').removeAttr('disabled', 'disabled');
        $('#Fata').removeAttr('disabled', 'disabled');
        $('#GilgitBaltistan').removeAttr('disabled', 'disabled');
        $('#Punjab').removeAttr('disabled', 'disabled');
        $('#Sindh').removeAttr('disabled', 'disabled');
    }
});
$("#Punjab").change(function () {
    if ($(this).prop('checked')) {
        $('#AzadKashmir').attr('disabled', 'disabled');
        $('#Balochistan').attr('disabled', 'disabled');
        $('#Fata').attr('disabled', 'disabled');
        $('#GilgitBaltistan').attr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
        $('#Sindh').attr('disabled', 'disabled');
    } else {
        $('#AzadKashmir').removeAttr('disabled', 'disabled');
        $('#Balochistan').removeAttr('disabled', 'disabled');
        $('#Fata').removeAttr('disabled', 'disabled');
        $('#GilgitBaltistan').removeAttr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').removeAttr('disabled', 'disabled');
        $('#Sindh').removeAttr('disabled', 'disabled');
    }
});
$("#Sindh").change(function () {
    if ($(this).prop('checked')) {
        $('#AzadKashmir').attr('disabled', 'disabled');
        $('#Balochistan').attr('disabled', 'disabled');
        $('#Fata').attr('disabled', 'disabled');
        $('#GilgitBaltistan').attr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').attr('disabled', 'disabled');
        $('#Punjab').attr('disabled', 'disabled');
    } else {
        $('#AzadKashmir').removeAttr('disabled', 'disabled');
        $('#Balochistan').removeAttr('disabled', 'disabled');
        $('#Fata').removeAttr('disabled', 'disabled');
        $('#GilgitBaltistan').removeAttr('disabled', 'disabled');
        $('#KhyberPakhtunkhwa').removeAttr('disabled', 'disabled');
        $('#Punjab').removeAttr('disabled', 'disabled');
    }
});

if (document.getElementById('RightThumb').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('RightIndex').checked) {
    $('#RightThumb').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('RightMiddle').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('RightRing').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('RightLittle').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('LeftThumb').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('LeftIndex').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('LeftMiddle').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('LeftRing').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
    $('#LeftLittle').attr('disabled', 'disabled');
}
if (document.getElementById('LeftLittle').checked) {
    $('#RightIndex').attr('disabled', 'disabled');
    $('#RightMiddle').attr('disabled', 'disabled');
    $('#RightRing').attr('disabled', 'disabled');
    $('#RightLittle').attr('disabled', 'disabled');
    $('#LeftThumb').attr('disabled', 'disabled');
    $('#LeftIndex').attr('disabled', 'disabled');
    $('#LeftMiddle').attr('disabled', 'disabled');
    $('#LeftRing').attr('disabled', 'disabled');
    $('#RightThumb').attr('disabled', 'disabled');
}

$("#RightThumb").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#RightIndex").change(function () {
    if ($(this).prop('checked')) {
        $('#RightThumb').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#RightMiddle").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#RightRing").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#RightLittle").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#LeftThumb").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#LeftIndex").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#LeftMiddle").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#LeftRing").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
        $('#LeftLittle').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
        $('#LeftLittle').removeAttr('disabled', 'disabled');
    }
});
$("#LeftLittle").change(function () {
    if ($(this).prop('checked')) {
        $('#RightIndex').attr('disabled', 'disabled');
        $('#RightMiddle').attr('disabled', 'disabled');
        $('#RightRing').attr('disabled', 'disabled');
        $('#RightLittle').attr('disabled', 'disabled');
        $('#LeftThumb').attr('disabled', 'disabled');
        $('#LeftIndex').attr('disabled', 'disabled');
        $('#LeftMiddle').attr('disabled', 'disabled');
        $('#LeftRing').attr('disabled', 'disabled');
        $('#RightThumb').attr('disabled', 'disabled');
    } else {
        $('#RightIndex').removeAttr('disabled', 'disabled');
        $('#RightMiddle').removeAttr('disabled', 'disabled');
        $('#RightRing').removeAttr('disabled', 'disabled');
        $('#RightLittle').removeAttr('disabled', 'disabled');
        $('#LeftThumb').removeAttr('disabled', 'disabled');
        $('#LeftIndex').removeAttr('disabled', 'disabled');
        $('#LeftMiddle').removeAttr('disabled', 'disabled');
        $('#LeftRing').removeAttr('disabled', 'disabled');
        $('#RightThumb').removeAttr('disabled', 'disabled');
    }
});

//#endregion